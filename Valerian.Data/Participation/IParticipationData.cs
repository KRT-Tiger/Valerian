﻿namespace Valerian.Data.Participation;

public interface IParticipationData
{
    DateOnly EventDate { get; }
    string EventName { get; }
    string Organizer { get; }
    IReadOnlyList<string> Participants { get; }
}