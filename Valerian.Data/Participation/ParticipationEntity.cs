﻿namespace Valerian.Data.Participation;

public class ParticipationEntity : EntityBase, IParticipationData
{
    public string EventName { get; set; } = string.Empty;
    public DateOnly EventDate { get; set; }
    public string Organizer { get; set; } = string.Empty;
    public List<string> Participants { get; set; } = [];

    IReadOnlyList<string> IParticipationData.Participants => Participants;
}
