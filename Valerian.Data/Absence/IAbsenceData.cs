﻿namespace Valerian.Data.Absence;

public interface IAbsenceData
{
    string MemberName { get; }
    DateOnly StartDate { get; }
    DateOnly EndDate { get; }
    string Reason { get; }
}