﻿namespace Valerian.Data.Absence;

public class AbsenceEntity : EntityBase, IAbsenceData
{
    public string MemberName { get; set; } = string.Empty;
    public DateOnly StartDate { get; set; }
    public DateOnly EndDate { get; set; }
    public string Reason { get; set; } = string.Empty;
}
