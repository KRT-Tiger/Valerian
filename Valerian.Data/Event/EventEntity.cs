﻿namespace Valerian.Data.Event;

public class EventEntity : EntityBase, IEventData
{
    public string EventName { get; set; } = string.Empty;
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Organizer { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public Guid WeeklyId { get; set; }
}
