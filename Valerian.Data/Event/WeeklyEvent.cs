﻿namespace Valerian.Data.Event;

public class WeeklyEvent
{
    public Guid Id { get; set; }
    public DayOfWeek DayOfWeek { get; set; }
    public TimeOnly Time { get; set; }
    public int Hours { get; set; }
    public string Name { get; set; } = string.Empty;
    public string[] Organizer { get; set; } = [];
    public string[] Excluded { get; set; } = [];
    public string Url { get; set; } = string.Empty;
}
