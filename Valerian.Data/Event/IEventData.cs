﻿namespace Valerian.Data.Event;

public interface IEventData
{
    string EventName { get; }
    DateTime StartDate { get; }
    DateTime EndDate { get; }
    string Organizer { get; }
    string Url { get; }
    Guid WeeklyId { get; }
}