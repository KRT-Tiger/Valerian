﻿namespace Valerian.Data.Event;

public class EventSettings
{
    public string[] EventNames { get; set; } = [];
    public string[] Organizer { get; set; } = [];
    public WeeklyEvent[] WeeklyEvents { get; set; } = [];
}
