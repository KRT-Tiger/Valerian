﻿namespace Valerian.Data.Account;

public static class DefaultRoles
{
    public const string Admin = nameof(Admin);
    public const string Officer = nameof(Officer);
    public const string Member = nameof(Member);

    public static string[] GetRoles()
    {
        return [Admin, Officer, Member];
    }
}
