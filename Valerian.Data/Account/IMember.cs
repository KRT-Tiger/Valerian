﻿using Valerian.Data.Group;

namespace Valerian.Data.Account;

public interface IMember
{
    string? MemberName { get; }
    string? CommandGroup { get; }
}
