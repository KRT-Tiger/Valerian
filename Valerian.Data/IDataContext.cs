﻿using Microsoft.EntityFrameworkCore;
using Valerian.Data.Absence;
using Valerian.Data.Event;
using Valerian.Data.Participation;

namespace Valerian.Data;

public interface IDataContext
{
    DbSet<AbsenceEntity> Absences { get; set; }
    DbSet<EventEntity> Events { get; set; }
    DbSet<ParticipationEntity> Participations { get; set; }
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}
