﻿namespace Valerian.Data.Group;

public sealed class Group
{
    public string Key { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
}
