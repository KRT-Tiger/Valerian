﻿namespace Valerian.Data.Group;

public class GroupSettings
{
    public Group[] CommandGroups { get; set; } = [];
    public Group[] OtherGroups { get; set; } = [];

    public Group? GetGroup(string key)
    {
        return CommandGroups
            .Union(OtherGroups)
            .SingleOrDefault(g => g.Key == key);
    }

    public IEnumerable<Group> GetGroups(params string[] except)
    {
        return CommandGroups
            .Union(OtherGroups)
            .Where(i => !except.Contains(i.Key));
    }
}
