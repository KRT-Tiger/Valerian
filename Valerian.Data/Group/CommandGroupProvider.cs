﻿namespace Valerian.Data.Group;

public class CommandGroupProvider(GroupSettings groupSettings)
{
    private readonly GroupSettings _groupSettings = groupSettings;

    public Group? GetByKey(string key)
    {
        return _groupSettings.CommandGroups
            .SingleOrDefault(x => x.Key == key);
    }
}
