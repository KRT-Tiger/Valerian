﻿using Microsoft.Extensions.DependencyInjection;
using Valerian.Common.Composition;
using Valerian.Data.Absence;
using Valerian.Data.Event;
using Valerian.Data.Group;
using Valerian.Data.Participation;

namespace Valerian.Data;

public sealed class _data : IRoot
{
    public void Compose(IServiceCollection services)
    {
        services.AddTransient<AbsenceEntity>();
        services.AddTransient<EventEntity>();
        services.AddTransient<ParticipationEntity>();
    }
}
