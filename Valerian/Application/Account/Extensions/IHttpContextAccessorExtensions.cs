﻿using System.Security.Principal;

namespace Valerian.Application.Accounts.Extensions;

public static class IHttpContextAccessorExtensions
{
    public static IIdentity GetUser(this IHttpContextAccessor accessor)
    {
        return accessor.HttpContext?.User.Identity ?? new GenericIdentity(string.Empty);
    }
}
