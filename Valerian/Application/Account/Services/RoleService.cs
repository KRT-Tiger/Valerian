﻿using Microsoft.AspNetCore.Identity;
using Valerian.Data.Account;

namespace Valerian.Application.Accounts.Services;

public class RoleService(RoleManager<IdentityRole> roleManager) : IRoleService
{
    readonly RoleManager<IdentityRole> _roleManager = roleManager;

    public async Task EnsureDefaultRoles()
    {
        foreach (var roleName in DefaultRoles.GetRoles())
        {
            if (await _roleManager.FindByNameAsync(roleName) == null)
            {
                var role = new IdentityRole(roleName);
                await _roleManager.CreateAsync(role);
            }
        }
    }
}
