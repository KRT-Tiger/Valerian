﻿using Microsoft.AspNetCore.Identity;
using Valerian.Data;
using Valerian.Data.Account;

namespace Valerian.Application.Accounts.Services;

public class UserService(UserManager<ApplicationUser> userManager, IServiceProvider services) : IUserService
{
    readonly UserManager<ApplicationUser> _userManager = userManager;
    readonly IServiceProvider _services = services;

    public async Task SetCommandGroup(string? username, string? commandGroup)
    {
        using var scope = _services.CreateAsyncScope();
        var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        var user = context.Users.Single(u => u.UserName == username);
        user.CommandGroup = commandGroup;
        await context.SaveChangesAsync();
    }

    public async Task EnsureDefaultUsers()
    {
        const string DEFAULT_USER_NAME = "YokoArashi";
        const string DEFAULT_USER_EMAIL = "roland.ockenfels@googlemail.com";

        var defaultUser = await _userManager.FindByEmailAsync(DEFAULT_USER_EMAIL) ?? await _userManager.FindByNameAsync(DEFAULT_USER_NAME);
        if (defaultUser == null)
        {
            defaultUser = new ApplicationUser(DEFAULT_USER_NAME, DEFAULT_USER_EMAIL)
            {
                EmailConfirmed = true
            };
            await _userManager.CreateAsync(defaultUser);
            await _userManager.AddPasswordAsync(defaultUser, "Kennwort1!");

            foreach (var roleName in DefaultRoles.GetRoles())
            {
                await _userManager.AddToRoleAsync(defaultUser, roleName);
            }
        }
        else
        {
            defaultUser.UserName = DEFAULT_USER_NAME;
            defaultUser.Email = DEFAULT_USER_EMAIL;
            await _userManager.UpdateAsync(defaultUser);

            var userRoles = await _userManager.GetRolesAsync(defaultUser);
            foreach (var roleName in DefaultRoles.GetRoles())
            {
                if (!userRoles.Contains(roleName))
                {
                    await _userManager.AddToRoleAsync(defaultUser, roleName);
                }
            }
        }
    }
}
