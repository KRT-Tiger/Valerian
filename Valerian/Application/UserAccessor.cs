﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Principal;

namespace Valerian.Application;

public sealed class UserAccessor(AuthenticationStateProvider authenticationStateProvider)
{
    private static readonly IIdentity Unknown = new GenericIdentity("Unkown");

    readonly AuthenticationStateProvider _authenticationStateProvider = authenticationStateProvider;

    public async Task<IIdentity> GetUser()
    {
        var state = await _authenticationStateProvider.GetAuthenticationStateAsync();
        return state.User.Identity ?? Unknown;
    }
}
