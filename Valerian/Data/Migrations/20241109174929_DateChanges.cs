﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Valerian.Migrations
{
    /// <inheritdoc />
    public partial class DateChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Until",
                table: "Absences",
                newName: "EndDate");

            migrationBuilder.RenameColumn(
                name: "From",
                table: "Absences",
                newName: "StartDate");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EndDate",
                table: "Absences",
                newName: "Until");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "Absences",
                newName: "From");
        }
    }
}
