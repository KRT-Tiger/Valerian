﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Valerian.Migrations
{
    /// <inheritdoc />
    public partial class ChangeNameToMemberName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Events",
                newName: "EventName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Absences",
                newName: "MemberName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EventName",
                table: "Events",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "MemberName",
                table: "Absences",
                newName: "Name");
        }
    }
}
