﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Valerian.Migrations
{
    /// <inheritdoc />
    public partial class CommandGroup : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CommandGroup",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommandGroup",
                table: "AspNetUsers");
        }
    }
}
