using Microsoft.AspNetCore.Identity;

namespace Valerian.Data.Account;

public class ApplicationUser : IdentityUser, IMember
{
    public ApplicationUser()
    {
    }

    public ApplicationUser(string userName, string email) : base(userName)
    {
        Email = email;
    }

    public string? CommandGroup { get; set; }

    string? IMember.MemberName => UserName;
}
