﻿using Valerian.Application;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.ParticipationListRequest;

namespace Valerian.Components.Participation.Models;

public sealed class ParticipationListModel(
    UserAccessor userAccessor,
    IAction<ParticipationListRequest, ListResult> participationListService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<ParticipationListRequest, ListResult> _participationListService = participationListService;

    public DateTime StartDate { get; set; } = DateTime.Today.AddMonths(-1);
    public DateTime EndDate { get; set; } = DateTime.Today;
    public List<ParticipationListItem> Items { get; } = [];

    public async Task Load()
    {
        var result = await ParticipationListRequest().With(_participationListService);
        Items.Clear();
        Items.AddRange(result.Items);
    }

    private async Task<ParticipationListRequest> ParticipationListRequest()
    {
        var startDate = DateOnly.FromDateTime(StartDate);
        var endDate = DateOnly.FromDateTime(EndDate);
        return new ParticipationListRequest(await _userAccessor.GetUser(), startDate, endDate);
    }
}
