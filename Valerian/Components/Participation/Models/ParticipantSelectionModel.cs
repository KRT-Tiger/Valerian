﻿
namespace Valerian.Components.Participation.Models;

public abstract class ParticipantSelectionModel
{
    public List<string> Candidates { get; } = [];
    public List<string> Participants { get; } = [];

    public IEnumerable<IGrouping<string, string>> GetCandidates()
    {
        return Candidates
            .GroupBy(i => i[..1].ToUpper())
            .OrderBy(i => i.Key);
    }

    public IEnumerable<IGrouping<string, string>> GetParticipants()
    {
        return Participants
            .GroupBy(i => i[..1].ToUpper())
            .OrderBy(i => i.Key);
    }

    public Task Select(string name)
    {
        if (Candidates.Remove(name))
        {
            Participants.Add(name);
        }
        else if (Participants.Remove(name))
        {
            Candidates.Add(name);
        }
        return Task.CompletedTask;
    }
}
