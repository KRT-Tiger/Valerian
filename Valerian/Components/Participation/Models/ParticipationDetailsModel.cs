﻿using Valerian.Application;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Requests;
using Valerian.Data.Account;
using static Valerian.Application.Participation.Requests.DeleteParticipationRequest;
using static Valerian.Application.Participation.Requests.ParticipationDetailsRequest;
using static Valerian.Application.Participation.Requests.UpdateParticipationRequest;

namespace Valerian.Components.Participation.Models;

public sealed class ParticipationDetailsModel(
    UserAccessor userAccessor,
    MemberSettings memberSettings,
    IAction<ParticipationDetailsRequest, DetailsResult> participationDetailsService,
    IAction<UpdateParticipationRequest, UpdateResult> updateParticipantsService,
    IAction<DeleteParticipationRequest, DeleteResult> deleteParticipationService) : ParticipantSelectionModel
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly MemberSettings _memberSettings = memberSettings;
    private readonly IAction<ParticipationDetailsRequest, DetailsResult> _participationDetailsService = participationDetailsService;
    private readonly IAction<UpdateParticipationRequest, UpdateResult> _updateParticipantsService = updateParticipantsService;
    private readonly IAction<DeleteParticipationRequest, DeleteResult> _deleteParticipationService = deleteParticipationService;

    public bool WasDeleted { get; set; } = false;
    public string SuccessMessage { get; set; } = string.Empty;
    public string ErrorMessage { get; set; } = string.Empty;

    public Guid Id { get; set; } = Guid.Empty;
    public string EventName { get; set; } = string.Empty;
    public DateTime EventDate { get; set; } = DateTime.Today;
    public string Organizer { get; set; } = string.Empty;

    public async Task Load()
    {
        var result = await ParticipationDetailsRequest().With(_participationDetailsService);
        EventName = result.Item.EventName;
        EventDate = result.Item.EventDate.ToDateTime(TimeOnly.MinValue);
        Organizer = result.Item.Organizer;
        Candidates.Clear();
        Candidates.AddRange(_memberSettings.MemberNames);
        Participants.Clear();
        Participants.AddRange(result.Item.Participants);
        Candidates.RemoveAll(s => result.Item.Participants.Contains(s));

        switch (result.Error)
        {
            case DetailsResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Anwesenheit nicht gefunden.";
                break;
            case DetailsResultState.Success:
                SuccessMessage = string.Empty;
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Save()
    {
        var result = await UpdateParticipationRequest().With(_updateParticipantsService);
        switch (result.State)
        {
            case UpdateResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Anwesenheit nicht gefunden.";
                break;
            case UpdateResultState.Success:
                SuccessMessage = "Die Anwesenheit wurde erfolgreich gespeichert.";
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Delete()
    {
        var result = await DeleteParticipationRequest().With(_deleteParticipationService);
        switch (result.State)
        {
            case DeleteResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Anwesenheit nicht gefunden.";
                break;
            case DeleteResultState.Success:
                SuccessMessage = "Die Anwesenheit wurde erfolgreich gelöscht.";
                ErrorMessage = string.Empty;
                WasDeleted = true;
                break;
        }
    }

    public Task ParticipantSelectionChanged()
    {
        SuccessMessage = string.Empty;
        ErrorMessage = string.Empty;
        return Task.CompletedTask;
    }

    private async Task<ParticipationDetailsRequest> ParticipationDetailsRequest()
    {
        return new ParticipationDetailsRequest(await _userAccessor.GetUser(), Id);
    }

    private async Task<UpdateParticipationRequest> UpdateParticipationRequest()
    {
        return new UpdateParticipationRequest(await _userAccessor.GetUser(), Id, [.. Participants]);
    }

    private async Task<DeleteParticipationRequest> DeleteParticipationRequest()
    {
        return new DeleteParticipationRequest(await _userAccessor.GetUser(), Id);
    }
}
