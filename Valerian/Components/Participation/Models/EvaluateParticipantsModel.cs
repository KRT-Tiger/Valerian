﻿using Valerian.Application;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Requests;
using Valerian.Data.Account;
using static Valerian.Application.Participation.Requests.EvaluateParticipantsRequest;

namespace Valerian.Components.Participation.Models;

public sealed class EvaluateParticipantsModel(
    OfficerSettings officerSettings,
    MemberSettings memberSettings,
    UserAccessor userAccessor,
    IAction<EvaluateParticipantsRequest, EvaluateResult> evaluateParticipantsService)
{
    private readonly OfficerSettings _officerSettings = officerSettings;
    private readonly MemberSettings _memberSettings = memberSettings;
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<EvaluateParticipantsRequest, EvaluateResult> _evaluateParticipantsService = evaluateParticipantsService;

    public List<EvaluateParticipantsItem> Items { get; } = [];

    public string GetRowStyle(EvaluateParticipantsItem item)
    {
        if (item.LastEventDate == DateOnly.MinValue && item.AbsenceUntil == DateOnly.MinValue)
        {
            return "alert-secondary";
        }
        var today = DateTime.Today;
        if (item.AbsenceUntil.ToDateTime(TimeOnly.MinValue) >= today)
        {
            return "alert-info";
        }
        var date = item.LastEventDate;
        if (date < item.AbsenceUntil)
        {
            date = item.AbsenceUntil;
        }
        var days = (today - date.ToDateTime(TimeOnly.MinValue)).Days;
        if (days > 180)
        {
            return "alert-danger";
        }
        if (days > 90)
        {
            return "alert-warning";
        }
        if (_officerSettings.OfficerNames.Contains(item.ParticipantName))
        {
            return "alert-primary";
        }
        return "alert-success";
    }

    public Task Sort(string propertyName)
    {
        var comparer = CreateComparer(propertyName);
        Items.Sort(comparer);
        return Task.CompletedTask;
    }

    public async Task Load()
    {
        var result = await EvaluateParticipantsRequest().With(_evaluateParticipantsService);
        Items.Clear();
        Items.AddRange(result.Items);
    }

    private async Task<EvaluateParticipantsRequest> EvaluateParticipantsRequest()
    {
        return new EvaluateParticipantsRequest(await _userAccessor.GetUser(), _memberSettings.MemberNames);
    }

    private static EvaluateParticipantItemComparer CreateComparer(string propertyName)
    {
        return new EvaluateParticipantItemComparer(propertyName);
    }

    private class EvaluateParticipantItemComparer(string propertyName) : IComparer<EvaluateParticipantsItem>
    {
        private readonly string _propertyName = propertyName;

        public int Compare(EvaluateParticipantsItem? x, EvaluateParticipantsItem? y)
        {
            var result = 0;
            if (x == null || y == null) return result;
            switch (_propertyName)
            {
                case nameof(EvaluateParticipantsItem.ParticipantName):
                    return x.ParticipantName.CompareTo(y.ParticipantName);
                case nameof(EvaluateParticipantsItem.NumberOfParticipations):
                    result = y.NumberOfParticipations.CompareTo(x.NumberOfParticipations);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationCurrentMonth):
                    result = y.ParticipationCurrentMonth.CompareTo(x.ParticipationCurrentMonth);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationPreviousMonth):
                    result = y.ParticipationPreviousMonth.CompareTo(x.ParticipationPreviousMonth);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationCurrentQuarter):
                    result = y.ParticipationCurrentQuarter.CompareTo(x.ParticipationCurrentQuarter);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationPreviousQuarter):
                    result = y.ParticipationPreviousQuarter.CompareTo(x.ParticipationPreviousQuarter);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationCurrentYear):
                    result = y.ParticipationCurrentYear.CompareTo(x.ParticipationCurrentYear);
                    break;
                case nameof(EvaluateParticipantsItem.ParticipationPreviousYear):
                    result = y.ParticipationPreviousYear.CompareTo(x.ParticipationPreviousYear);
                    break;
                case nameof(EvaluateParticipantsItem.AbsenceUntil):
                    result = y.AbsenceUntil.CompareTo(x.AbsenceUntil);
                    break;
            }
            if (result == 0)
            {
                result = y.LastEventDate.CompareTo(x.LastEventDate);
            }
            if (result == 0)
            {
                result = x.ParticipantName.CompareTo(y.ParticipantName);
            }
            return result;
        }
    }
}
