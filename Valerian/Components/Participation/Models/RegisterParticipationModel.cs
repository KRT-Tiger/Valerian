﻿using Valerian.Application;
using Valerian.Application.Event.Requests;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;
using Valerian.Data.Account;
using Valerian.Data.Event;
using static Valerian.Application.Event.Requests.EventDetailsRequest;
using static Valerian.Application.Participation.Requests.CreateParticipationRequest;

namespace Valerian.Components.Participation.Models;

public class RegisterParticipationModel(
    UserAccessor userAccessor,
    MemberSettings memberSettings,
    EventSettings eventSettings,
    IAction<CreateParticipationRequest, CreateResult> createParticipationService,
    IAction<EventDetailsRequest, DetailsResult> eventDetailsService) : ParticipantSelectionModel
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly MemberSettings _memberSettings = memberSettings;
    private readonly EventSettings _eventSettings = eventSettings;
    private readonly IAction<CreateParticipationRequest, CreateResult> _createParticipationService = createParticipationService;
    private readonly IAction<EventDetailsRequest, DetailsResult> _eventDetailsService = eventDetailsService;

    public bool WasSaved { get; set; } = false;
    public string ErrorMessage { get; set; } = string.Empty;
    public bool CanEditEventName { get; set; } = false;

    public Guid Id { get; set; } = Guid.Empty;
    public string EventName { get; set; } = string.Empty;
    public DateTime EventDate { get; set; } = DateTime.Today;
    public string Organizer { get; set; } = string.Empty;

    public async Task Load(DateTime? date, string? name, string? organizer, Guid? eventId)
    {
        Id = Guid.Empty;
        EventName = _eventSettings.WeeklyEvents.SingleOrDefault(i => i.DayOfWeek == DateTime.Today.DayOfWeek)?.Name ?? string.Empty;
        EventDate = DateTime.Today;
        Organizer = _eventSettings.Organizer[0];
        Candidates.Clear();
        Candidates.AddRange(_memberSettings.MemberNames);
        Participants.Clear();

        if (date != null)
        {
            EventDate = date.Value;
        }
        if (name != null)
        {
            EventName = name.Trim();
        }
        if (organizer != null)
        {
            Organizer = organizer.Trim();
        }
        if (eventId != null)
        {
            var request = new EventDetailsRequest(await _userAccessor.GetUser(), eventId.Value);
            var result = await request.With(_eventDetailsService);
            if (result.State == DetailsResultState.Success)
            {
                Id = eventId.Value;
                EventName = result.Item.EventName;
                EventDate = result.Item.StartDate;
                Organizer = result.Item.Organizer;
            }
            else
            {
                ErrorMessage = "Die angegebene Veranstaltung existiert nicht.";
            }
        }
    }

    public async Task Save()
    {
        var result = await CreateParticipationRequest().With(_createParticipationService);
        switch (result.State)
        {
            case CreateResultState.AlreadyEntered:
                ErrorMessage = "Die Anwesenheit ist bereits eingetragen.";
                WasSaved = false;
                Id = result.Id;
                break;
            case CreateResultState.EventNameRequired:
                ErrorMessage = "Bitte gebe den Veranstaltungsnamen ein.";
                WasSaved = false;
                Id = Guid.Empty;
                break;
            case CreateResultState.ParticipantRequired:
                ErrorMessage = "Bitte wähle mindestens einen Teilnehmer aus.";
                WasSaved = false;
                Id = Guid.Empty;
                break;
            case CreateResultState.Success:
                ErrorMessage = string.Empty;
                WasSaved = true;
                Id = result.Id;
                break;
        }
    }

    public Task ParticipantSelectionChanged()
    {
        WasSaved = false;
        ErrorMessage = string.Empty;
        return Task.CompletedTask;
    }

    private async Task<CreateParticipationRequest> CreateParticipationRequest()
    {
        var item = new ParticipationDetailsItem(EventName.Trim(), DateOnly.FromDateTime(EventDate), Organizer.Trim(), [.. Participants]);
        return new CreateParticipationRequest(await _userAccessor.GetUser(), Id, item);
    }
}
