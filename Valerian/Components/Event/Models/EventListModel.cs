﻿using Valerian.Application;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.EventListRequest;

namespace Valerian.Components.Event.Models;

public class EventListModel(UserAccessor userAccessor, IAction<EventListRequest, ListResult> eventListService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<EventListRequest, ListResult> _eventListService = eventListService;

    public DateTime StartDate { get; set; } = DateTime.Today.AddMonths(-1);
    public DateTime EndDate { get; set; } = DateTime.Today.AddMonths(2);
    public List<EventListItem> Items { get; } = [];

    public async Task Load()
    {
        var result = await EventListRequest().With(_eventListService);
        Items.Clear();
        Items.AddRange(result.Items);
    }

    private async Task<EventListRequest> EventListRequest()
    {
        return new EventListRequest(await _userAccessor.GetUser(), StartDate, EndDate);
    }
}
