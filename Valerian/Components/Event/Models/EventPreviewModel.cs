﻿using Valerian.Application;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.EventPreviewRequest;

namespace Valerian.Components.Event.Models;

public sealed class EventPreviewModel(
    UserAccessor userAccessor,
    IAction<EventPreviewRequest, PreviewResult> eventPreviewService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<EventPreviewRequest, PreviewResult> _eventPreviewService = eventPreviewService;

    public List<EventPreviewItem> Items { get; } = [];

    public async Task Load()
    {
        var result = await EventPreviewRequest().With(_eventPreviewService);
        Items.Clear();
        Items.AddRange(result.Items);
    }

    private async Task<EventPreviewRequest> EventPreviewRequest()
    {
        var startDate = DateTime.Today.AddDays(-7).ToDateOnly();
        var endDate = DateTime.Today.AddMonths(1).ToDateOnly();
        return new EventPreviewRequest(await _userAccessor.GetUser(), startDate, endDate);
    }
}
