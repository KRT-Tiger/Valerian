﻿using Valerian.Application;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;
using Valerian.Data.Event;
using static Valerian.Application.Event.Requests.CreateEventRequest;

namespace Valerian.Components.Event.Models;

public class RegisterEventModel(UserAccessor userAccessor,
                                EventSettings eventSettings,
                                IAction<CreateEventRequest, CreateResult> createEventService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly EventSettings _eventSettings = eventSettings;
    private readonly IAction<CreateEventRequest, CreateResult> _createEventService = createEventService;

    public bool WasSaved { get; set; } = false;
    public string ErrorMessage { get; set; } = string.Empty;
    public bool CanEditEventName { get; set; } = false;

    public Guid Id { get; set; } = Guid.Empty;
    public string EventName { get; set; } = string.Empty;

    private DateTime _startDate;
    public DateTime StartDate
    {
        get => _startDate;
        set
        {
            _startDate = value;
            if (_startDate > _endDate && _startDate < DateTime.MaxValue.AddHours(-2))
            {
                _endDate = _startDate.AddHours(2);
            }
        }
    }

    private DateTime _endDate;
    public DateTime EndDate
    {
        get => _endDate;
        set
        {
            _endDate = value;
            if (_endDate < _startDate && _endDate > DateTime.MinValue.AddHours(2))
            {
                _startDate = _endDate.AddHours(-2);
            }
        }
    }

    public string Organizer { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public Guid WeeklyId { get; set; }

    public Task Load(DateTime? date, string? name, string? organizer, Guid? weeklyId)
    {
        Id = Guid.Empty;
        EventName = "Neue Veranstaltung";

        _startDate = DateTime.Now;
        _endDate = DateTime.Now.AddHours(2);

        Organizer = _eventSettings.Organizer[0];
        Url = string.Empty;
        WeeklyId = Guid.Empty;

        if (date != null)
        {
            _startDate = date.Value;
            _endDate = date.Value.AddHours(2);
        }
        if (name != null)
        {
            EventName = name.Trim();
        }
        if (organizer != null)
        {
            Organizer = organizer.Trim();
        }
        if (weeklyId != null)
        {
            var weekly = _eventSettings.WeeklyEvents.Single(i => i.Id == weeklyId.Value);

            _startDate = StartDate.ToDateOnly().ToDateTime(weekly.Time);
            _endDate = StartDate.AddHours(weekly.Hours);

            Url = weekly.Url;
            WeeklyId = weekly.Id;
        }
        else
        {
            var weekly = _eventSettings.WeeklyEvents.SingleOrDefault(i => i.DayOfWeek == StartDate.DayOfWeek);
            if (weekly != null)
            {
                EventName = _eventSettings.WeeklyEvents.SingleOrDefault(i => i.DayOfWeek == DateTime.Today.DayOfWeek)?.Name ?? string.Empty;
            }

        }
        return Task.CompletedTask;
    }

    public async Task Save()
    {
        var result = await CreateEventRequest().With(_createEventService);
        switch (result.State)
        {
            case CreateResultState.AlreadyEntered:
                ErrorMessage = "Die Veranstaltung ist bereits eingetragen.";
                WasSaved = false;
                Id = result.Id;
                break;
            case CreateResultState.EventNameRequired:
                ErrorMessage = "Bitte gebe den Veranstaltungsnamen ein.";
                WasSaved = false;
                Id = Guid.Empty;
                break;
            case CreateResultState.Success:
                ErrorMessage = string.Empty;
                WasSaved = true;
                Id = result.Id;
                break;
        }
    }

    private async Task<CreateEventRequest> CreateEventRequest()
    {
        var item = new EventDetailsItem(EventName.Trim(), StartDate, EndDate, Organizer.Trim(), Url.Trim(), WeeklyId, Guid.Empty);
        return new CreateEventRequest(await _userAccessor.GetUser(), item);
    }
}
