﻿using Valerian.Application;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Requests;
using Valerian.Data.Group;
using static Valerian.Application.Event.Requests.DeleteEventRequest;
using static Valerian.Application.Event.Requests.EventDetailsRequest;
using static Valerian.Application.Event.Requests.UpdateEventRequest;

namespace Valerian.Components.Event.Models;

public class EventDetailsModel(UserAccessor userAccessor,
                               IAction<EventDetailsRequest, DetailsResult> eventDetailsService,
                               IAction<UpdateEventRequest, UpdateResult> updateParticipantsService,
                               IAction<DeleteEventRequest, DeleteResult> deleteEventService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<EventDetailsRequest, DetailsResult> _eventDetailsService = eventDetailsService;
    private readonly IAction<UpdateEventRequest, UpdateResult> _updateParticipantsService = updateParticipantsService;
    private readonly IAction<DeleteEventRequest, DeleteResult> _deleteEventService = deleteEventService;

    public bool WasDeleted { get; set; } = false;
    public string SuccessMessage { get; set; } = string.Empty;
    public string ErrorMessage { get; set; } = string.Empty;

    public Guid Id { get; set; }
    public string EventName { get; set; } = string.Empty;

    private DateTime _startDate;
    public DateTime StartDate
    {
        get => _startDate;
        set
        {
            _startDate = value;
            if (_startDate > _endDate && _startDate < DateTime.MaxValue.AddHours(-2))
            {
                _endDate = _startDate.AddHours(2);
            }
        }
    }

    private DateTime _endDate;
    public DateTime EndDate
    {
        get => _endDate;
        set
        {
            _endDate = value;
            if (_endDate < _startDate && _endDate > DateTime.MinValue.AddHours(2))
            {
                _startDate = _endDate.AddHours(-2);
            }
        }
    }

    public string Organizer { get; set; } = string.Empty;
    public string Url { get; set; } = string.Empty;
    public Guid WeeklyId { get; set; }
    public Guid ParticipationId { get; set; }

    public async Task Load()
    {
        var result = await EventDetailsRequest().With(_eventDetailsService);
        EventName = result.Item.EventName;
        
        _startDate = result.Item.StartDate;
        _endDate = result.Item.EndDate;
        
        Organizer = result.Item.Organizer;
        Url = result.Item.Url;
        WeeklyId = result.Item.WeeklyId;
        ParticipationId = result.Item.ParticipationId;

        switch (result.State)
        {
            case DetailsResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Veranstaltung nicht gefunden.";
                break;
            case DetailsResultState.Success:
                SuccessMessage = string.Empty;
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Save()
    {
        var result = await UpdateEventRequest().With(_updateParticipantsService);
        switch (result.State)
        {
            case UpdateResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Veranstaltung nicht gefunden.";
                break;
            case UpdateResultState.Success:
                SuccessMessage = "Die Veranstaltung wurde erfolgreich gespeichert.";
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Delete()
    {
        var result = await DeleteEventRequest().With(_deleteEventService);
        switch (result.State)
        {
            case DeleteResultState.NotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Veranstaltung nicht gefunden.";
                break;
            case DeleteResultState.Success:
                SuccessMessage = "Die Veranstaltung wurde erfolgreich gelöscht.";
                ErrorMessage = string.Empty;
                WasDeleted = true;
                break;
        }
    }

    public Task ParticipantSelectionChanged()
    {
        SuccessMessage = string.Empty;
        ErrorMessage = string.Empty;
        return Task.CompletedTask;
    }

    private async Task<EventDetailsRequest> EventDetailsRequest()
    {
        return new EventDetailsRequest(await _userAccessor.GetUser(), Id);
    }

    private async Task<UpdateEventRequest> UpdateEventRequest()
    {
        var item = new EventDetailsItem(EventName, StartDate, EndDate, Organizer, Url, WeeklyId, ParticipationId);
        return new UpdateEventRequest(await _userAccessor.GetUser(), Id, item);
    }

    private async Task<DeleteEventRequest> DeleteEventRequest()
    {
        return new DeleteEventRequest(await _userAccessor.GetUser(), Id);
    }
}
