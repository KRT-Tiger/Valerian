﻿using Valerian.Application;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;

namespace Valerian.Components.Absence.Models;

public class RegisterAbsenceModel(
    UserAccessor userAccessor,
    IAction<CreateAbsenceRequest, CreateAbsenceResponse> createAbsenceService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<CreateAbsenceRequest, CreateAbsenceResponse> _createAbsenceService = createAbsenceService;

    public string ErrorMessage { get; set; } = string.Empty;
    public bool WasSaved { get; set; } = false;

    public Guid Id { get; set; } = Guid.Empty;
    public string MemberName { get; set; } = string.Empty;
    public DateTime StartDate { get; set; } = DateTime.Today;
    public DateTime EndDate { get; set; } = DateTime.Today.AddDays(7);
    public string Reason { get; set; } = string.Empty;

    public Task Load(string memberName)
    {
        WasSaved = false;
        MemberName = memberName;
        StartDate = DateTime.Today;
        EndDate = DateTime.Today.AddDays(7);
        Reason = string.Empty;
        return Task.CompletedTask;
    }

    public async Task Save()
    {
        var result = await CreateAbsenceRequest().With(_createAbsenceService);
        switch (result.Error)
        {
            case CreateAbsenceError.NameRequired:
                ErrorMessage = "Bitte wähle ein Mitglied aus.";
                WasSaved = false;
                Id = Guid.Empty;
                break;
            case CreateAbsenceError.ReasonRequired:
                ErrorMessage = "Bitte gebe den Grund der Abwesenheit ein.";
                WasSaved = false;
                Id = Guid.Empty;
                break;
            case CreateAbsenceError.None:
                ErrorMessage = string.Empty;
                WasSaved = true;
                Id = result.AbsenceId;
                break;
        }
    }

    private async Task<CreateAbsenceRequest> CreateAbsenceRequest()
    {
        var memberName = MemberName.Trim();
        var startDate = StartDate.ToDateOnly();
        var endDate = EndDate.ToDateOnly();
        var reason = Reason.Trim();
        return new CreateAbsenceRequest(await _userAccessor.GetUser(), memberName, startDate, endDate, reason);
    }
}
