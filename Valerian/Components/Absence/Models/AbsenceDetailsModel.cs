﻿using Valerian.Application;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;
using static Valerian.Application.Absence.Requests.AbsenceDetailsRequest;
using static Valerian.Application.Absence.Requests.DeleteAbsenceRequest;
using static Valerian.Application.Absence.Requests.UpdateAbsenceRequest;

namespace Valerian.Components.Absence.Models;

public class AbsenceDetailsModel(
    UserAccessor userAccessor,
    IAction<AbsenceDetailsRequest, DetailsResult> absenceDetailsRequest,
    IAction<UpdateAbsenceRequest, UpdateResult> updateAbsenceService,
    IAction<DeleteAbsenceRequest, DeleteResult> deleteAbsenceService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<AbsenceDetailsRequest, DetailsResult> _absenceDetailsRequest = absenceDetailsRequest;
    private readonly IAction<UpdateAbsenceRequest, UpdateResult> _updateAbsenceService = updateAbsenceService;
    private readonly IAction<DeleteAbsenceRequest, DeleteResult> _deleteAbsenceService = deleteAbsenceService;

    public string SuccessMessage { get; set; } = string.Empty;
    public string ErrorMessage { get; set; } = string.Empty;
    public bool WasDeleted { get; set; } = false;

    public Guid Id { get; set; } = Guid.Empty;
    public string Name { get; set; } = string.Empty;
    public DateTime StartDate { get; set; } = DateTime.Today;
    public DateTime EndDate { get; set; } = DateTime.Today.AddDays(7);
    public string Reason { get; set; } = string.Empty;

    public async Task Load()
    {
        var result = await AbsenceDetailsRequest().With(_absenceDetailsRequest);
        Name = result.MemberName;
        StartDate = result.StartDate.ToDateTime(TimeOnly.MinValue);
        EndDate = result.EndDate.ToDateTime(TimeOnly.MinValue);
        Reason = result.Reason;

        switch (result.State)
        {
            case DetailsResultState.AbsenceNotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Abwesenheit nicht gefunden.";
                break;
            case DetailsResultState.Success:
                SuccessMessage = string.Empty;
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Save()
    {
        var result = await UpdateAbsenceRequest().With(_updateAbsenceService);
        switch (result.State)
        {
            case UpdateResultState.AbsenceNotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Abwesenheit nicht gefunden.";
                break;
            case UpdateResultState.Success:
                SuccessMessage = "Die Abwesenheit wurde erfolgreich gespeichert.";
                ErrorMessage = string.Empty;
                break;
        }
    }

    public async Task Delete()
    {
        var result = await DeleteAbsenceRequest().With(_deleteAbsenceService);
        switch (result.State)
        {
            case DeleteResultState.AbsenceNotFound:
                SuccessMessage = string.Empty;
                ErrorMessage = "Abwesenheit nicht gefunden.";
                break;
            case DeleteResultState.Success:
                SuccessMessage = "Die Abwesenheit wurde erfolgreich gelöscht.";
                ErrorMessage = string.Empty;
                WasDeleted = true;
                break;
        }
    }

    private async Task<AbsenceDetailsRequest> AbsenceDetailsRequest()
    {
        return new AbsenceDetailsRequest(await _userAccessor.GetUser(), Id);
    }

    private async Task<UpdateAbsenceRequest> UpdateAbsenceRequest()
    {
        return new UpdateAbsenceRequest(await _userAccessor.GetUser(), Id, Name, StartDate.ToDateOnly(), EndDate.ToDateOnly(), Reason);
    }

    private async Task<DeleteAbsenceRequest> DeleteAbsenceRequest()
    {
        return new DeleteAbsenceRequest(await _userAccessor.GetUser(), Id);
    }
}
