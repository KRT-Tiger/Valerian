﻿using Valerian.Application;
using Valerian.Application.Absence.Models;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Common.Requests;
using static Valerian.Application.Absence.Requests.AbsenceListRequest;

namespace Valerian.Components.Absence.Models;

public class AbsenceListModel(
    UserAccessor userAccessor,
    IAction<AbsenceListRequest, ListResult> absenceListService)
{
    private readonly UserAccessor _userAccessor = userAccessor;
    private readonly IAction<AbsenceListRequest, ListResult> _absenceListService = absenceListService;

    public DateTime StartDate { get; set; } = DateTime.Today.AddMonths(-1);
    public DateTime EndDate { get; set; } = DateTime.Today.AddMonths(2);
    public List<AbsenceListItem> Items { get; } = [];

    public async Task Load()
    {
        var result = await AbsenceListRequest().With(_absenceListService);
        Items.Clear();
        Items.AddRange(result.Items);
    }

    private async Task<AbsenceListRequest> AbsenceListRequest()
    {
        var startDate = StartDate.ToDateOnly();
        var endDate = EndDate.ToDateOnly();
        return new AbsenceListRequest(await _userAccessor.GetUser(), startDate, endDate);
    }
}
