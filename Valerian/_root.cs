﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Valerian.Application;
using Valerian.Application.Accounts.Services;
using Valerian.Common;
using Valerian.Common.Composition;
using Valerian.Components.Absence.Models;
using Valerian.Components.Account;
using Valerian.Components.Event.Models;
using Valerian.Components.Participation.Models;
using Valerian.Data;
using Valerian.Data.Account;
using Valerian.Data.Group;
using Valerian.Data.Event;

namespace Valerian;

public static class _root
{
    public static IServiceCollection AddComposition(this IServiceCollection services)
    {
        services.AddSingleton<IEmailSender<ApplicationUser>, IdentityNoOpEmailSender>();
        services.AddTransient<UserAccessor>();

        var composition = new _common().Combine(new _data(), new _application());
        composition.Compose(services);

        services.AddScoped<IDataContext, ApplicationDbContext>();
        services.AddScoped<IRoleService, RoleService>();
        services.AddScoped<IUserService, UserService>();

        services.AddOptions<MemberSettings>()
            .BindConfiguration(nameof(MemberSettings))
            .Validate(s => s.MemberNames.Length > 0)
            .ValidateOnStart();
        services.AddTransient(s => s.GetRequiredService<IOptions<MemberSettings>>().Value);

        services.AddOptions<OfficerSettings>()
            .BindConfiguration(nameof(OfficerSettings))
            .Validate(s => s.OfficerNames.Length > 0)
            .ValidateOnStart();
        services.AddTransient(s => s.GetRequiredService<IOptions<OfficerSettings>>().Value);

        services.AddOptions<EventSettings>()
            .BindConfiguration(nameof(EventSettings))
            .Validate(s => s.EventNames.Length > 0)
            .ValidateOnStart();
        services.AddTransient(s => s.GetRequiredService<IOptions<EventSettings>>().Value);

        services.AddOptions<GroupSettings>()
            .BindConfiguration(nameof(GroupSettings))
            .Validate(s => s.CommandGroups.Length == 4)
            .ValidateOnStart();
        services.AddTransient(s => s.GetRequiredService<IOptions<GroupSettings>>().Value);

        // Absence
        services.AddTransient<AbsenceDetailsModel>();
        services.AddTransient<AbsenceListModel>();
        services.AddTransient<RegisterAbsenceModel>();

        // Event
        services.AddTransient<EventPreviewModel>();
        services.AddTransient<EventDetailsModel>();
        services.AddTransient<EventListModel>();
        services.AddTransient<RegisterEventModel>();

        // Participation
        services.AddTransient<EvaluateParticipantsModel>();
        services.AddTransient<ParticipationDetailsModel>();
        services.AddTransient<ParticipationListModel>();
        services.AddTransient<RegisterParticipationModel>();

        return services;
    }
}
