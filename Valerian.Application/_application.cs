﻿using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Services;
using Valerian.Application.Accounts.Services;
using Valerian.Application.Event.Services;
using Valerian.Application.Participation.Services;
using Valerian.Common.Actions;
using Valerian.Common.Composition;

namespace Valerian.Application;

public sealed class _application : IRoot
{
    public void Compose(IServiceCollection services)
    {
        // Absence
        services.AddSingleton<CreateAbsenceService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<CreateAbsenceService>()));
        services.AddSingleton<DeleteAbsenceService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<DeleteAbsenceService>()));
        services.AddSingleton<AbsenceListService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<AbsenceListService>()));
        services.AddSingleton<AbsenceDetailsService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<AbsenceDetailsService>()));
        services.AddSingleton<UpdateAbsenceService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<UpdateAbsenceService>()));

        // Account
        services.AddSingleton<CheckHandleService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<CheckHandleService>()));

        // Event
        services.AddSingleton<EventPreviewService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<EventPreviewService>()));
        services.AddSingleton<CreateEventService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<CreateEventService>()));
        services.AddSingleton<DeleteEventService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<DeleteEventService>()));
        services.AddSingleton<EventListService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<EventListService>()));
        services.AddSingleton<EventDetailsService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<EventDetailsService>()));
        services.AddSingleton<UpdateEventService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<UpdateEventService>()));

        // Participation
        services.AddSingleton<CreateParticipationService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<CreateParticipationService>()));
        services.AddSingleton<DeleteParticipationService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<DeleteParticipationService>()));
        services.AddSingleton<EvaluateParticipantsService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<EvaluateParticipantsService>()));
        services.AddSingleton<ParticipationListService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<ParticipationListService>()));
        services.AddSingleton<ParticipationDetailsService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<ParticipationDetailsService>()));
        services.AddSingleton<UpdateParticipationService>();
        services.AddTransient(s => ActionDecorator.Create(s.GetRequiredService<UpdateParticipationService>()));
    }
}