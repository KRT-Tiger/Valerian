﻿namespace Valerian.Application.Participation.Models;

public sealed record EvaluateParticipantsItem
{
    private readonly string _participantName;
    private readonly int _numberOfParticipations;
    private readonly int _participationCurrentMonth;
    private readonly int _participationPreviousMonth;
    private readonly int _participationCurrentQuarter;
    private readonly int _participationPreviousQuarter;
    private readonly int _participationCurrentYear;
    private readonly int _participationPreviousYear;
    private readonly DateOnly _lastEventDate;
    private readonly DateOnly _lastAbsenceDate;

    public EvaluateParticipantsItem(string participantName,
                                    int numberOfParticipations,
                                    int participationCurrentMonth,
                                    int participationPreviousMonth,
                                    int participationCurrentQuarter,
                                    int participationPreviousQuarter,
                                    int participationCurrentYear,
                                    int participationPreviousYear,
                                    DateOnly lastEventDate,
                                    DateOnly lastAbsenceDate)
    {
        _participantName = participantName;
        _numberOfParticipations = numberOfParticipations;
        _participationCurrentMonth = participationCurrentMonth;
        _participationPreviousMonth = participationPreviousMonth;
        _participationCurrentQuarter = participationCurrentQuarter;
        _participationPreviousQuarter = participationPreviousQuarter;
        _participationCurrentYear = participationCurrentYear;
        _participationPreviousYear = participationPreviousYear;
        _lastEventDate = lastEventDate;
        _lastAbsenceDate = lastAbsenceDate;
    }

    public string ParticipantName => _participantName;
    public int NumberOfParticipations => _numberOfParticipations;
    public int ParticipationCurrentMonth => _participationCurrentMonth;
    public int ParticipationPreviousMonth => _participationPreviousMonth;
    public int ParticipationCurrentQuarter => _participationCurrentQuarter;
    public int ParticipationPreviousQuarter => _participationPreviousQuarter;
    public int ParticipationCurrentYear => _participationCurrentYear;
    public int ParticipationPreviousYear => _participationPreviousYear;
    public DateOnly LastEventDate => _lastEventDate;
    public DateOnly AbsenceUntil => _lastAbsenceDate;
}
