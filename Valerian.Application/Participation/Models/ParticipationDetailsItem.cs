﻿using Valerian.Data.Participation;

namespace Valerian.Application.Participation.Models;

public sealed record ParticipationDetailsItem : IParticipationData
{
    public static readonly ParticipationDetailsItem Empty = new(string.Empty, DateOnly.MinValue, string.Empty, []);

    private readonly string _eventName;
    private readonly DateOnly _eventDate;
    private readonly string _organizer;
    private readonly string[] _participants;

    public ParticipationDetailsItem(string eventName, DateOnly eventDate, string organizer, string[] participants)
    {
        _eventName = eventName;
        _eventDate = eventDate;
        _organizer = organizer;
        _participants = participants;
    }

    public string EventName => _eventName;
    public DateOnly EventDate => _eventDate;
    public string Organizer => _organizer;
    public IReadOnlyList<string> Participants => _participants;
}
