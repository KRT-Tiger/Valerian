﻿namespace Valerian.Application.Participation.Models;

public sealed record ParticipationListItem
{
    private readonly Guid _id;
    private readonly string _eventName;
    private readonly DateOnly _eventDate;
    private readonly string _organizer;
    private readonly int _numberOfParticipants;

    public ParticipationListItem(Guid id, string eventName, DateOnly eventDate, string organizer, int numberOfParticipants)
    {
        _id = id;
        _eventName = eventName;
        _eventDate = eventDate;
        _organizer = organizer;
        _numberOfParticipants = numberOfParticipants;
    }

    public Guid Id => _id;
    public string EventName => _eventName;
    public DateOnly EventDate => _eventDate;
    public string Organizer => _organizer;
    public int NumberOfParticipants => _numberOfParticipants;
}
