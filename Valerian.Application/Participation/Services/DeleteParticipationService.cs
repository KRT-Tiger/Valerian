﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Participation.Requests.DeleteParticipationRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class DeleteParticipationService(IServiceProvider services) : IAction<DeleteParticipationRequest, DeleteResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DeleteResult> Execute(DeleteParticipationRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var state = DeleteResultState.NotFound;
        var entity = await context.Participations
            .SingleOrDefaultAsync(p => p.Id == request.Id);

        if (entity != null)
        {
            state = DeleteResultState.Success;
            context.Participations.Remove(entity);
            await context.SaveChangesAsync();
        }

        return new DeleteResult(state);
    }
}
