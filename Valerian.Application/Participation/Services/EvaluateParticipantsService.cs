﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using Valerian.Data.Absence;
using Valerian.Data.Participation;
using static Valerian.Application.Participation.Requests.EvaluateParticipantsRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class EvaluateParticipantsService(IServiceProvider services) : IAction<EvaluateParticipantsRequest, EvaluateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<EvaluateResult> Execute(EvaluateParticipantsRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var today = DateTime.Today.ToDateOnly();
        var absences = await context.Absences
            .Where(p => p.StartDate <= today)
            .ToListAsync();
        var participations = await context.Participations
            .Where(p => p.EventDate <= today)
            .ToListAsync();
        var items = request.MemberNames
            .Select(p => CreateItem(p, participations, absences))
            .OrderBy(i => i.ParticipantName)
            .ToArray();
        return new EvaluateResult(items);
    }

    private static EvaluateParticipantsItem CreateItem(
        string participantName,
        IEnumerable<ParticipationEntity> participations,
        IEnumerable<AbsenceEntity> absences)
    {
        participations = participations.Where(p => p.Participants.Contains(participantName));
        absences = absences.Where(p => p.MemberName.Equals(participantName));
        return new EvaluateParticipantsItem(participantName,
                                            participations.Count(),
                                            CountParticipationsInCurrentMonth(participations),
                                            CountParticipationsInPreviousMonth(participations),
                                            CountParticipationsInCurrentQuarter(participations),
                                            CountParticipationsInPreviousQuarter(participations),
                                            CountParticipationsInCurrentYear(participations),
                                            CountParticipationsInPreviousYear(participations),
                                            GetLastEventDate(participations),
                                            GetAbsenceUntil(absences));
    }

    private static int CountParticipationsInPeriod(IEnumerable<ParticipationEntity> participations, DateOnly from, DateOnly until)
    {
        return participations.Count(p => p.EventDate >= from && p.EventDate <= until);
    }

    private static int CountParticipationsInCurrentMonth(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today;
        var from = new DateOnly(today.Year, today.Month, 1);
        var until = new DateOnly(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static int CountParticipationsInPreviousMonth(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today.AddMonths(-1);
        var from = new DateOnly(today.Year, today.Month, 1);
        var until = new DateOnly(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static int CountParticipationsInCurrentQuarter(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today;
        var from = GetBeginOfQuarter(today);
        var until = GetEndOfQuarter(today);
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static int CountParticipationsInPreviousQuarter(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today.AddMonths(-3);
        var from = GetBeginOfQuarter(today);
        var until = GetEndOfQuarter(today);
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static int CountParticipationsInCurrentYear(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today;
        var from = new DateOnly(today.Year, 1, 1);
        var until = new DateOnly(today.Year, 12, DateTime.DaysInMonth(today.Year, 12));
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static int CountParticipationsInPreviousYear(IEnumerable<ParticipationEntity> participations)
    {
        var today = DateTime.Today.AddYears(-1);
        var from = new DateOnly(today.Year, 1, 1);
        var until = new DateOnly(today.Year, 12, DateTime.DaysInMonth(today.Year, 12));
        return CountParticipationsInPeriod(participations, from, until);
    }

    private static DateOnly GetLastEventDate(IEnumerable<ParticipationEntity> participations)
    {
        var result = DateOnly.MinValue;
        if (participations.Any())
        {
            result = participations.Max(p => p.EventDate);
        }
        return result;
    }

    private static DateOnly GetAbsenceUntil(IEnumerable<AbsenceEntity> absences)
    {
        var result = DateOnly.MinValue;
        if (absences.Any())
        {
            result = absences.Max(p => p.EndDate);
        }
        return result;
    }

    private static DateOnly GetBeginOfQuarter(DateTime participations)
    {
        var month = participations.Month;
        if (month >= 1 && month <= 3)
        {
            month = 1;
        }
        else if (month >= 4 && month <= 6)
        {
            month = 4;
        }
        else if (month >= 7 && month <= 9)
        {
            month = 7;
        }
        else if (month >= 10 && month <= 12)
        {
            month = 10;
        }
        return new DateOnly(participations.Year, month, 1);
    }

    private static DateOnly GetEndOfQuarter(DateTime participations)
    {
        var month = participations.Month;
        if (month >= 1 && month <= 3)
        {
            month = 3;
        }
        else if (month >= 4 && month <= 6)
        {
            month = 6;
        }
        else if (month >= 7 && month <= 9)
        {
            month = 9;
        }
        else if (month >= 10 && month <= 12)
        {
            month = 12;
        }
        return new DateOnly(participations.Year, month, DateTime.DaysInMonth(participations.Year, month));
    }
}
