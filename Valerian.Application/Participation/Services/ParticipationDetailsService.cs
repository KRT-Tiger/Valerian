﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Participation.Requests.ParticipationDetailsRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class ParticipationDetailsService(IServiceProvider services) : IAction<ParticipationDetailsRequest, DetailsResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DetailsResult> Execute(ParticipationDetailsRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var state = DetailsResultState.NotFound;
        var item = ParticipationDetailsItem.Empty;
        var entity = await context.Participations
            .SingleOrDefaultAsync(p => p.Id == request.Id);

        if (entity != null)
        {
            state = DetailsResultState.Success;
            item = new ParticipationDetailsItem(entity.EventName, entity.EventDate, entity.Organizer, [.. entity.Participants]);
        }

        return new DetailsResult(state, item);
    }
}
