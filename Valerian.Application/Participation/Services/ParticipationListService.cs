﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Models;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Participation.Requests.ParticipationListRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class ParticipationListService(IServiceProvider services) : IAction<ParticipationListRequest, ListResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<ListResult> Execute(ParticipationListRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var participations = await context.Participations
            .Where(p => p.EventDate >= request.StartDate && p.EventDate <= request.EndDate)
            .OrderByDescending(p => p.EventDate).ThenBy(p => p.EventName)
            .ToListAsync();
        var items = participations
            .Select(p => new ParticipationListItem(p.Id, p.EventName, p.EventDate, p.Organizer, p.Participants.Count))
            .ToArray();
        return new ListResult(items);
    }
}
