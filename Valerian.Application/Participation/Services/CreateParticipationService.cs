﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using Valerian.Data.Participation;
using static Valerian.Application.Participation.Requests.CreateParticipationRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class CreateParticipationService(IServiceProvider services) : IAction<CreateParticipationRequest, CreateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<CreateResult> Execute(CreateParticipationRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.Item.EventName))
        {
            return new CreateResult(CreateResultState.EventNameRequired, Guid.Empty);
        }

        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var participation = await context.Participations
            .SingleOrDefaultAsync(p => p.EventName == request.Item.EventName && p.EventDate == request.Item.EventDate);

        if (participation == null && request.Id != Guid.Empty)
        {
            participation = await context.Participations
                .SingleOrDefaultAsync(p => p.Id == request.Id);
        }

        var state = CreateResultState.AlreadyEntered;
        if (participation == null)
        {
            state = CreateResultState.Success;
            participation = _services.GetRequiredService<ParticipationEntity>();

            participation.EventName = request.Item.EventName;
            participation.EventDate = request.Item.EventDate;
            participation.Organizer = request.Item.Organizer;
            participation.Participants = [.. request.Item.Participants];

            if (request.Id != Guid.Empty)
            {
                participation.Id = request.Id;
            }
            else
            {
                var startDate = participation.EventDate.ToDateTime(TimeOnly.MinValue);
                var endDate = participation.EventDate.ToDateTime(TimeOnly.MaxValue);
                var entity = await context.Events
                    .SingleOrDefaultAsync(e => e.EventName == participation.EventName
                                               && e.StartDate >= startDate
                                               && e.StartDate <= endDate);

                if (entity != null)
                {
                    participation.Id = entity.Id;
                    entity.Organizer = participation.Organizer;
                }
            }

            context.Participations.Add(participation);
            await context.SaveChangesAsync();
        }

        return new CreateResult(state, participation.Id);
    }
}
