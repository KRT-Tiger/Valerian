﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Participation.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using static Valerian.Application.Participation.Requests.UpdateParticipationRequest;

namespace Valerian.Application.Participation.Services;

internal sealed class UpdateParticipationService(IServiceProvider services) : IAction<UpdateParticipationRequest, UpdateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<UpdateResult> Execute(UpdateParticipationRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = UpdateResultState.NotFound;
        var participation = await context.Participations
            .SingleOrDefaultAsync(p => p.Id == request.Id);

        if (participation != null)
        {
            error = UpdateResultState.Success;
            participation.Participants = [.. request.Participants];

            var startDate = participation.EventDate.ToDateTime(TimeOnly.MinValue);
            var endDate = participation.EventDate.ToDateTime(TimeOnly.MaxValue);

            var entity = await context.Events
                .SingleOrDefaultAsync(e => e.Id == participation.Id);

            entity ??= await context.Events
                .SingleOrDefaultAsync(e => e.EventName == participation.EventName
                                           && e.StartDate >= startDate
                                           && e.StartDate <= endDate);

            if (entity != null)
            {
                participation.Id = entity.Id;
                participation.EventName = entity.EventName;
                participation.EventDate = entity.StartDate.ToDateOnly();
                participation.Organizer = entity.Organizer;
            }

            await context.SaveChangesAsync();
        }

        return new UpdateResult(error);
    }
}
