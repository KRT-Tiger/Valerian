﻿using System.Security.Principal;
using Valerian.Application.Participation.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.ParticipationListRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record ParticipationListRequest : Request, IRequest<ListResult>
{
    private readonly DateOnly _startDate;
    private readonly DateOnly _endDate;

    public ParticipationListRequest(IIdentity user, DateOnly startDate, DateOnly endDate) : base(user)
    {
        _startDate = startDate;
        _endDate = endDate;
    }

    public DateOnly StartDate => _startDate;
    public DateOnly EndDate => _endDate;

    public sealed record ListResult
    {
        private readonly ParticipationListItem[] _items;

        public ListResult(ParticipationListItem[] items)
        {
            _items = items;
        }

        public ParticipationListItem[] Items => _items;
    }
}
