﻿using System.Security.Principal;
using Valerian.Application.Participation.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.CreateParticipationRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record CreateParticipationRequest : Request, IRequest<CreateResult>
{
    private readonly Guid _id;
    private readonly ParticipationDetailsItem _item;

    public CreateParticipationRequest(IIdentity user, Guid id, ParticipationDetailsItem item) : base(user)
    {
        _id = id;
        _item = item;
    }

    public Guid Id => _id;
    public ParticipationDetailsItem Item => _item;

    public sealed record CreateResult
    {
        private readonly CreateResultState _state;
        private readonly Guid _id;

        public CreateResult(CreateResultState state, Guid id)
        {
            _state = state;
            _id = id;
        }

        public CreateResultState State => _state;
        public Guid Id => _id;
    }

    public enum CreateResultState
    {
        Success,
        AlreadyEntered,
        EventNameRequired,
        ParticipantRequired
    }
}