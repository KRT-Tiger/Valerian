﻿using System.Security.Principal;
using Valerian.Application.Participation.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.EvaluateParticipantsRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record EvaluateParticipantsRequest : Request, IRequest<EvaluateResult>
{
    private readonly string[] _memberNames;

    public EvaluateParticipantsRequest(IIdentity user, string[] memberNames) : base(user)
    {
        _memberNames = memberNames;
    }

    public string[] MemberNames => _memberNames;

    public sealed record EvaluateResult
    {
        private readonly EvaluateParticipantsItem[] _items;

        public EvaluateResult(EvaluateParticipantsItem[] items)
        {
            _items = items;
        }

        public EvaluateParticipantsItem[] Items => _items;
    }
}
