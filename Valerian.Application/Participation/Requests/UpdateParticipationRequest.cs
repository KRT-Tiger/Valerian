﻿using System.Security.Principal;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.UpdateParticipationRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record UpdateParticipationRequest : Request, IRequest<UpdateResult>
{
    private readonly Guid _id;
    private readonly string[] _participants;

    public UpdateParticipationRequest(IIdentity user, Guid id, string[] participants) : base(user)
    {
        _id = id;
        _participants = participants;
    }

    public Guid Id => _id;
    public string[] Participants => _participants;

    public sealed record UpdateResult
    {
        private readonly UpdateResultState _state;

        public UpdateResult(UpdateResultState state)
        {
            _state = state;
        }

        public UpdateResultState State => _state;
    }

    public enum UpdateResultState
    {
        Success,
        NotFound
    }
}