﻿using System.Security.Principal;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.DeleteParticipationRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record DeleteParticipationRequest : Request, IRequest<DeleteResult>
{
    private readonly Guid _id;

    public DeleteParticipationRequest(IIdentity user, Guid id) : base(user)
    {
        _id = id;
    }

    public Guid Id => _id;

    public sealed record DeleteResult
    {
        private readonly DeleteResultState _state;

        public DeleteResult(DeleteResultState state)
        {
            _state = state;
        }

        public DeleteResultState State => _state;
    }

    public enum DeleteResultState
    {
        Success,
        NotFound
    }
}
