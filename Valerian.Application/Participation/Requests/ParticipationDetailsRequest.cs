﻿using System.Security.Principal;
using Valerian.Application.Participation.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Participation.Requests.ParticipationDetailsRequest;

namespace Valerian.Application.Participation.Requests;

public sealed partial record ParticipationDetailsRequest : Request, IRequest<DetailsResult>
{
    private readonly Guid _id;

    public ParticipationDetailsRequest(IIdentity user, Guid id) : base(user)
    {
        _id = id;
    }

    public Guid Id => _id;

    public sealed record DetailsResult
    {
        private readonly DetailsResultState _error;
        private readonly ParticipationDetailsItem _item;

        public DetailsResult(DetailsResultState error, ParticipationDetailsItem item)
        {
            _error = error;
            _item = item;
        }

        public DetailsResultState Error => _error;
        public ParticipationDetailsItem Item => _item;
    }

    public enum DetailsResultState
    {
        Success,
        NotFound
    }
}