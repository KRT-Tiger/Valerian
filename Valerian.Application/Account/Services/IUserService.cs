﻿namespace Valerian.Application.Accounts.Services;

public interface IUserService
{
    Task SetCommandGroup(string? username, string? commandGroup);
    Task EnsureDefaultUsers();
}
