﻿namespace Valerian.Application.Accounts.Services;

public interface IRoleService
{
    Task EnsureDefaultRoles();
}
