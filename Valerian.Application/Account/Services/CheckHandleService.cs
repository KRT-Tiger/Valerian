﻿using System.Net;
using Valerian.Application.Accounts.Requests;
using Valerian.Common.Actions;

namespace Valerian.Application.Accounts.Services;

internal sealed class CheckHandleService : IAction<CheckHandleRequest, bool>
{
    public async Task<bool> Execute(CheckHandleRequest request)
    {
        var url = $"https://robertsspaceindustries.com/citizens/{request.Handle}";
        using var client = new HttpClient();
        try
        {
            var result = await client.GetStringAsync(url);
            return result.Contains(request.BioCode);
        }
        catch (HttpRequestException ex)
        {
            if (ex.StatusCode == HttpStatusCode.NotFound)
            {
                return false;
            }
            throw;
        }
    }
}
