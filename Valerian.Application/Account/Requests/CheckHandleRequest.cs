﻿using System.Security.Principal;
using Valerian.Common.Requests;

namespace Valerian.Application.Accounts.Requests;

public sealed record CheckHandleRequest : Request, IRequest<bool>
{
    readonly string _handle;
    readonly string _bioCode;

    public CheckHandleRequest(IIdentity user, string handle, string bioCode) : base(user)
    {
        _handle = handle;
        _bioCode = bioCode;
    }

    public string Handle => _handle;

    public string BioCode => _bioCode;
}
