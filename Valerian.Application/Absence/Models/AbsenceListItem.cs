﻿using Valerian.Data.Absence;

namespace Valerian.Application.Absence.Models;

public sealed record AbsenceListItem : IAbsenceData
{
    private readonly Guid _id;
    private readonly string _name;
    private readonly DateOnly _from;
    private readonly DateOnly _until;
    private readonly string _reason;

    public AbsenceListItem(Guid id, string name, DateOnly from, DateOnly until, string reason)
    {
        _id = id;
        _name = name;
        _from = from;
        _until = until;
        _reason = reason;
    }

    public Guid Id => _id;
    public string MemberName => _name;
    public DateOnly StartDate => _from;
    public DateOnly EndDate => _until;
    public string Reason => _reason;
}
