﻿using System.Security.Principal;
using Valerian.Common.Requests;
using Valerian.Data.Absence;

namespace Valerian.Application.Absence.Requests;

public record CreateAbsenceRequest : Request, IRequest<CreateAbsenceResponse>, IAbsenceData
{
    private readonly string _name;
    private readonly DateOnly _from;
    private readonly DateOnly _until;
    private readonly string _reason;

    public CreateAbsenceRequest(IIdentity user, string name, DateOnly from, DateOnly until, string reason) : base(user)
    {
        _name = name;
        _from = from;
        _until = until;
        _reason = reason;
    }

    public string MemberName => _name;
    public DateOnly StartDate => _from;
    public DateOnly EndDate => _until;
    public string Reason => _reason;
}

public record CreateAbsenceResponse
{
    private readonly CreateAbsenceError _error;
    private readonly Guid _absenceId;

    public CreateAbsenceResponse(CreateAbsenceError error, Guid absenceId)
    {
        _error = error;
        _absenceId = absenceId;
    }

    public CreateAbsenceError Error => _error;
    public Guid AbsenceId => _absenceId;
}

public enum CreateAbsenceError
{
    None,
    NameRequired,
    ReasonRequired
}
