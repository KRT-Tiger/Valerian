﻿using System.Security.Principal;
using Valerian.Common.Requests;
using static Valerian.Application.Absence.Requests.DeleteAbsenceRequest;

namespace Valerian.Application.Absence.Requests;

public record DeleteAbsenceRequest : Request, IRequest<DeleteResult>
{
    private readonly Guid _absenceId;

    public DeleteAbsenceRequest(IIdentity user, Guid absenceId) : base(user)
    {
        _absenceId = absenceId;
    }

    public Guid AbsenceId => _absenceId;

    public record DeleteResult
    {
        private readonly DeleteResultState _state;

        public DeleteResult(DeleteResultState state)
        {
            _state = state;
        }

        public DeleteResultState State => _state;
    }

    public enum DeleteResultState
    {
        Success,
        AbsenceNotFound
    }
}
