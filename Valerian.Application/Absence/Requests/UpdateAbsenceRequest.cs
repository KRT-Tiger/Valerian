﻿using System.Security.Principal;
using Valerian.Common.Requests;
using Valerian.Data.Absence;
using static Valerian.Application.Absence.Requests.UpdateAbsenceRequest;

namespace Valerian.Application.Absence.Requests;

public sealed partial record UpdateAbsenceRequest : Request, IRequest<UpdateResult>, IAbsenceData
{
    private readonly Guid _absenceId;
    private readonly string _name;
    private readonly DateOnly _from;
    private readonly DateOnly _until;
    private readonly string _reason;

    public UpdateAbsenceRequest(IIdentity user, Guid absenceId, string name, DateOnly from, DateOnly until, string reason) : base(user)
    {
        _absenceId = absenceId;
        _name = name;
        _from = from;
        _until = until;
        _reason = reason;
    }

    public Guid AbsenceId => _absenceId;
    public string MemberName => _name;
    public DateOnly StartDate => _from;
    public DateOnly EndDate => _until;
    public string Reason => _reason;

    public sealed record UpdateResult
    {
        private readonly UpdateResultState _error;

        public UpdateResult(UpdateResultState error)
        {
            _error = error;
        }

        public UpdateResultState State => _error;
    }

    public enum UpdateResultState
    {
        Success,
        AbsenceNotFound
    }
}
