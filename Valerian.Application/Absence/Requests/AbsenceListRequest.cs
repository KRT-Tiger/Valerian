﻿using System.Security.Principal;
using Valerian.Application.Absence.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Absence.Requests.AbsenceListRequest;

namespace Valerian.Application.Absence.Requests;

public sealed partial record AbsenceListRequest : Request, IRequest<ListResult>
{
    private readonly DateOnly _startDate;
    private readonly DateOnly _endDate;

    public AbsenceListRequest(IIdentity user, DateOnly startDate, DateOnly endDate) : base(user)
    {
        _startDate = startDate;
        _endDate = endDate;
    }

    public DateOnly StartDate => _startDate;
    public DateOnly EndDate => _endDate;

    public sealed record ListResult
    {
        private readonly AbsenceListItem[] _items;

        public ListResult(AbsenceListItem[] items)
        {
            _items = items;
        }

        public AbsenceListItem[] Items => _items;
    }
}
