﻿using System.Security.Principal;
using Valerian.Common.Requests;
using Valerian.Data.Absence;
using static Valerian.Application.Absence.Requests.AbsenceDetailsRequest;

namespace Valerian.Application.Absence.Requests;

public record AbsenceDetailsRequest : Request, IRequest<DetailsResult>
{
    private readonly Guid _absenceId;

    public AbsenceDetailsRequest(IIdentity user, Guid absenceId) : base(user)
    {
        _absenceId = absenceId;
    }

    public Guid AbsenceId => _absenceId;

    public record DetailsResult : IAbsenceData
    {
        private readonly DetailsResultState _state;
        private readonly string _name;
        private readonly DateOnly _from;
        private readonly DateOnly _until;
        private readonly string _reason;

        public DetailsResult(DetailsResultState state, string name, DateOnly from, DateOnly until, string reason)
        {
            _state = state;
            _name = name;
            _from = from;
            _until = until;
            _reason = reason;
        }

        public DetailsResultState State => _state;
        public string MemberName => _name;
        public DateOnly StartDate => _from;
        public DateOnly EndDate => _until;
        public string Reason => _reason;
    }

    public enum DetailsResultState
    {
        Success,
        AbsenceNotFound
    }
}