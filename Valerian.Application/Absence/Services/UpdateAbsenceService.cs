﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Absence.Requests.UpdateAbsenceRequest;

namespace Valerian.Application.Absence.Services;

internal sealed class UpdateAbsenceService(IServiceProvider services) : IAction<UpdateAbsenceRequest, UpdateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<UpdateResult> Execute(UpdateAbsenceRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = UpdateResultState.AbsenceNotFound;
        var item = await context.Absences.SingleOrDefaultAsync(p => p.Id == request.AbsenceId);

        if (item != null)
        {
            error = UpdateResultState.Success;
            item.MemberName = request.MemberName;
            item.StartDate = request.StartDate;
            item.EndDate = request.EndDate;
            item.Reason = request.Reason;

            await context.SaveChangesAsync();
        }

        return new UpdateResult(error);
    }
}
