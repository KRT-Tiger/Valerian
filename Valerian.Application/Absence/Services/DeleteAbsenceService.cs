﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Absence.Requests.DeleteAbsenceRequest;

namespace Valerian.Application.Absence.Services;

internal sealed class DeleteAbsenceService(IServiceProvider services) : IAction<DeleteAbsenceRequest, DeleteResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DeleteResult> Execute(DeleteAbsenceRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = DeleteResultState.AbsenceNotFound;
        var item = await context.Absences.SingleOrDefaultAsync(p => p.Id == request.AbsenceId);

        if (item != null)
        {
            error = DeleteResultState.Success;
            context.Absences.Remove(item);
            await context.SaveChangesAsync();
        }

        return new DeleteResult(error);
    }
}
