﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Models;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Absence.Requests.AbsenceListRequest;

namespace Valerian.Application.Absence.Services;

internal sealed class AbsenceListService(IServiceProvider services) : IAction<AbsenceListRequest, ListResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<ListResult> Execute(AbsenceListRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var result = await context.Absences
            .Where(i => i.EndDate >= request.StartDate && i.StartDate <= request.EndDate)
            .OrderByDescending(p => p.StartDate).ThenBy(p => p.EndDate)
            .ToListAsync();
        var items = result
            .Select(i => new AbsenceListItem(i.Id, i.MemberName, i.StartDate, i.EndDate, i.Reason))
            .ToArray();
        return new ListResult(items);
    }
}
