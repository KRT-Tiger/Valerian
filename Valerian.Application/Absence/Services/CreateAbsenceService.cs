﻿using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using Valerian.Data.Absence;

namespace Valerian.Application.Absence.Services;

internal sealed class CreateAbsenceService(IServiceProvider services) : IAction<CreateAbsenceRequest, CreateAbsenceResponse>
{
    private readonly IServiceProvider _services = services;

    public async Task<CreateAbsenceResponse> Execute(CreateAbsenceRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.MemberName))
        {
            return new CreateAbsenceResponse(CreateAbsenceError.NameRequired, Guid.Empty);
        }

        if (string.IsNullOrWhiteSpace(request.Reason))
        {
            return new CreateAbsenceResponse(CreateAbsenceError.ReasonRequired, Guid.Empty);
        }

        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = CreateAbsenceError.None;
        var entity = _services.GetRequiredService<AbsenceEntity>();
        entity.MemberName = request.MemberName;
        entity.StartDate = request.StartDate;
        entity.EndDate = request.EndDate;
        entity.Reason = request.Reason;

        context.Absences.Add(entity);
        await context.SaveChangesAsync();

        return new CreateAbsenceResponse(error, entity.Id);
    }
}
