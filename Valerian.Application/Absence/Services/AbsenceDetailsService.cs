﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Absence.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Absence.Requests.AbsenceDetailsRequest;

namespace Valerian.Application.Absence.Services;

internal sealed class AbsenceDetailsService(IServiceProvider services) : IAction<AbsenceDetailsRequest, DetailsResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DetailsResult> Execute(AbsenceDetailsRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = DetailsResultState.AbsenceNotFound;
        var name = string.Empty;
        var from = DateOnly.MinValue;
        var until = DateOnly.MaxValue;
        var reason = string.Empty;

        var item = await context.Absences.SingleOrDefaultAsync(p => p.Id == request.AbsenceId);

        if (item != null)
        {
            error = DetailsResultState.Success;
            name = item.MemberName;
            from = item.StartDate;
            until = item.EndDate;
            reason = item.Reason;
        }

        return new DetailsResult(error, name, from, until, reason);
    }
}
