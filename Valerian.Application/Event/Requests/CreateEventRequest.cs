﻿using System.Security.Principal;
using Valerian.Application.Event.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.CreateEventRequest;

namespace Valerian.Application.Event.Requests;

public sealed partial record CreateEventRequest : Request, IRequest<CreateResult>
{
    private readonly EventDetailsItem _item;

    public CreateEventRequest(IIdentity user, EventDetailsItem item) : base(user)
    {
        _item = item;
    }

    public EventDetailsItem Item => _item;

    public sealed record CreateResult
    {
        private readonly CreateResultState _state;
        private readonly Guid _id;

        public CreateResult(CreateResultState state, Guid id)
        {
            _state = state;
            _id = id;
        }

        public CreateResultState State => _state;
        public Guid Id => _id;
    }

    public enum CreateResultState
    {
        Success,
        AlreadyEntered,
        EventNameRequired
    }
}