﻿using System.Security.Principal;
using Valerian.Application.Event.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.UpdateEventRequest;

namespace Valerian.Application.Event.Requests;

public sealed partial record UpdateEventRequest : Request, IRequest<UpdateResult>
{
    private readonly Guid _eventId;
    private readonly EventDetailsItem _item;

    public UpdateEventRequest(IIdentity user, Guid id, EventDetailsItem item) : base(user)
    {
        _eventId = id;
        _item = item;
    }

    public Guid Id => _eventId;
    public EventDetailsItem Item => _item;

    public sealed record UpdateResult
    {
        private readonly UpdateResultState _state;

        public UpdateResult(UpdateResultState state)
        {
            _state = state;
        }

        public UpdateResultState State => _state;
    }

    public enum UpdateResultState
    {
        Success,
        NotFound
    }
}