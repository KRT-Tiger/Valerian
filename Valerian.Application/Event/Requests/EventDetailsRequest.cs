﻿using System.Security.Principal;
using Valerian.Application.Event.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.EventDetailsRequest;

namespace Valerian.Application.Event.Requests;

public sealed partial record EventDetailsRequest : Request, IRequest<DetailsResult>
{
    private readonly Guid _id;

    public EventDetailsRequest(IIdentity user, Guid id) : base(user)
    {
        _id = id;
    }

    public Guid Id => _id;

    public sealed record DetailsResult
    {
        private readonly DetailsResultState _error;
        private readonly EventDetailsItem _item;

        public DetailsResult(DetailsResultState error, EventDetailsItem item)
        {
            _error = error;
            _item = item;
        }

        public DetailsResultState State => _error;
        public EventDetailsItem Item => _item;
    }

    public enum DetailsResultState
    {
        Success,
        NotFound
    }
}