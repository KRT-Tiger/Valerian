﻿using System.Security.Principal;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.DeleteEventRequest;

namespace Valerian.Application.Event.Requests;

public sealed partial record DeleteEventRequest : Request, IRequest<DeleteResult>
{
    private readonly Guid _id;

    public DeleteEventRequest(IIdentity user, Guid id) : base(user)
    {
        _id = id;
    }

    public Guid Id => _id;

    public sealed record DeleteResult
    {
        private readonly DeleteResultState _state;

        public DeleteResult(DeleteResultState state)
        {
            _state = state;
        }

        public DeleteResultState State => _state;
    }

    public enum DeleteResultState
    {
        Success,
        NotFound
    }
}
