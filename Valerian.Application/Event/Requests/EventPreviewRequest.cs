﻿using System.Security.Principal;
using Valerian.Application.Event.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.EventPreviewRequest;

namespace Valerian.Application.Event.Requests;

public record EventPreviewRequest : Request, IRequest<PreviewResult>
{
    private readonly DateOnly _startDate;
    private readonly DateOnly _endDate;

    public EventPreviewRequest(IIdentity user, DateOnly startDate, DateOnly endDate) : base(user)
    {
        _startDate = startDate;
        _endDate = endDate;
    }

    public DateOnly StartDate => _startDate;
    public DateOnly EndDate => _endDate;

    public record PreviewResult
    {
        private readonly EventPreviewItem[] _items;

        public PreviewResult(EventPreviewItem[] items)
        {
            _items = items;
        }

        public EventPreviewItem[] Items => _items;
    }
}
