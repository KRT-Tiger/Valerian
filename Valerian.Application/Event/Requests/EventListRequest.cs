﻿using System.Security.Principal;
using Valerian.Application.Event.Models;
using Valerian.Common.Requests;
using static Valerian.Application.Event.Requests.EventListRequest;

namespace Valerian.Application.Event.Requests;

public sealed partial record EventListRequest : Request, IRequest<ListResult>
{
    private readonly DateTime _startDate;
    private readonly DateTime _endDate;

    public EventListRequest(IIdentity user, DateTime startDate, DateTime endDate) : base(user)
    {
        _startDate = startDate;
        _endDate = endDate;
    }

    public DateTime StartDate => _startDate;
    public DateTime EndDate => _endDate;

    public sealed record ListResult
    {
        private readonly EventListItem[] _items;

        public ListResult(EventListItem[] items)
        {
            _items = items;
        }

        public EventListItem[] Items => _items;
    }
}
