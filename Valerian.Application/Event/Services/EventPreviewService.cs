﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using Valerian.Data.Event;
using static Valerian.Application.Event.Requests.EventPreviewRequest;

namespace Valerian.Application.Event.Services;

internal class EventPreviewService(IServiceProvider services, EventSettings eventSettings) : IAction<EventPreviewRequest, PreviewResult>
{
    private readonly IServiceProvider _services = services;
    private readonly EventSettings _eventSettings = eventSettings;

    public async Task<PreviewResult> Execute(EventPreviewRequest request)
    {
        if (request.StartDate > request.EndDate)
        {
            throw new InvalidOperationException("The start date cannot be later than the end date.");
        }
        using var scope = _services.CreateScope();

        var requestStartDate = request.StartDate.ToDateTime(TimeOnly.MinValue);
        var requestEndDate = request.EndDate.ToDateTime(TimeOnly.MaxValue);

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var events = await context.Events
            .Where(e => e.StartDate <= requestEndDate && e.EndDate >= requestStartDate)
            .ToListAsync();
        var eventIds = events.Select(i => i.Id).ToArray();
        var participations = await context.Participations
            .Where(i => eventIds.Contains(i.Id))
            .ToListAsync();
        var eventsOnDate = events
            .GroupBy(i => i.StartDate.ToDateOnly())
            .ToDictionary(k => k.Key, v =>
            {
                var items = v.Select(i =>
                {
                    var participation = participations
                        .SingleOrDefault(p => p.Id == i.Id);

                    return new EventPreviewItem(i.Id,
                                                i.EventName,
                                                i.Organizer,
                                                i.StartDate,
                                                i.EndDate,
                                                i.Url,
                                                i.WeeklyId,
                                                participation?.Id ?? Guid.Empty);
                });
                return items.ToList();
            });

        var currentDate = request.StartDate;
        var lastOrganizer = new Dictionary<string, string>();

        while (currentDate <= request.EndDate)
        {
            if (!eventsOnDate.TryGetValue(currentDate, out List<EventPreviewItem>? itemsOnDate))
            {
                itemsOnDate = [];
                eventsOnDate.Add(currentDate, itemsOnDate);
            }

            var weeklyEvent = _eventSettings.WeeklyEvents
                .SingleOrDefault(i => i.DayOfWeek == currentDate.DayOfWeek);

            if (weeklyEvent != null)
            {
                if (!lastOrganizer.TryGetValue(weeklyEvent.Name, out string? value))
                {
                    var lastEntry = await context.Participations
                        .OrderByDescending(i => i.EventDate)
                        .FirstOrDefaultAsync(i => i.EventName == weeklyEvent.Name && i.EventDate < currentDate);
                    value = lastEntry?.Organizer ?? weeklyEvent.Organizer.Last();
                    lastOrganizer.Add(weeklyEvent.Name, value);
                }

                var item = itemsOnDate
                    .SingleOrDefault(i => i.WeeklyId == weeklyEvent.Id);
                if (item != null)
                {
                    lastOrganizer[weeklyEvent.Name] = item.Organizer;
                }
                else
                {
                    var startDate = currentDate.ToDateTime(weeklyEvent.Time);
                    var endDate = startDate.AddHours(weeklyEvent.Hours);
                    var count = CountDayOfWeek(startDate);
                    var organizer = weeklyEvent.Organizer[count - 1];
                    var participation = await context.Participations
                        .SingleOrDefaultAsync(i => i.EventName == weeklyEvent.Name && i.EventDate == currentDate);

                    if (participation != null)
                    {
                        organizer = participation.Organizer;
                    }
                    else if (organizer.Contains('*'))
                    {
                        var lastName = value;
                        var digits = string.Empty;
                        foreach (var c in lastName)
                        {
                            if (char.IsDigit(c))
                            {
                                digits += c;
                            }
                        }
                        if (!int.TryParse(digits, out int number))
                        {
                            number = 0;
                        }
                        number++;
                        if (number > 4) number = 1;
                        var name = organizer.Replace("*", number.ToString());
                        while (weeklyEvent.Excluded.Contains(name))
                        {
                            number++;
                            if (number > 4) number = 1;
                            name = organizer.Replace("*", number.ToString());
                        }
                        organizer = name;
                    }
                    lastOrganizer[weeklyEvent.Name] = organizer;

                    item = new EventPreviewItem(Guid.Empty,
                                                weeklyEvent.Name,
                                                organizer,
                                                startDate,
                                                endDate,
                                                weeklyEvent.Url,
                                                weeklyEvent.Id,
                                                participation?.Id ?? Guid.Empty);
                    itemsOnDate.Add(item);
                }
            }
            currentDate = currentDate.AddDays(1);
        }
        var items = eventsOnDate.Values
            .SelectMany(i => i)
            .OrderBy(i => i.StartDate).ThenBy(i => i.EndDate)
            .ToArray();
        return new PreviewResult(items);
    }

    private static int CountDayOfWeek(DateTime date)
    {
        var result = 0;
        var currentDate = date.BeginOfMonth();
        while (currentDate <= date)
        {
            if (currentDate.DayOfWeek == date.DayOfWeek)
            {
                result++;
            }
            currentDate = currentDate.AddDays(1);
        }
        return result;
    }
}
