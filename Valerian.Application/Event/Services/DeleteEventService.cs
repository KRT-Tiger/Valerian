﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Event.Requests.DeleteEventRequest;

namespace Valerian.Application.Event.Services;

internal sealed class DeleteEventService(IServiceProvider services) : IAction<DeleteEventRequest, DeleteResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DeleteResult> Execute(DeleteEventRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var state = DeleteResultState.NotFound;
        var entity = await context.Events
            .SingleOrDefaultAsync(p => p.Id == request.Id);

        if (entity != null)
        {
            state = DeleteResultState.Success;
            context.Events.Remove(entity);
            await context.SaveChangesAsync();
        }

        return new DeleteResult(state);
    }
}
