﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Event.Requests.EventListRequest;

namespace Valerian.Application.Event.Services;

internal sealed class EventListService(IServiceProvider services) : IAction<EventListRequest, ListResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<ListResult> Execute(EventListRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var events = await context.Events
            .Where(i => i.StartDate >= request.StartDate && i.EndDate <= request.EndDate)
            .OrderByDescending(p => p.StartDate).ThenBy(p => p.EventName)
            .ToListAsync();
        var eventIds = events.Select(i => i.Id).ToArray();
        var participations = await context.Participations
            .Where(i => eventIds.Contains(i.Id))
            .ToListAsync();
        var items = events
            .Select(i =>
            {
                var participation = participations
                    .SingleOrDefault(p => p.Id == i.Id);
                return new EventListItem(i.Id,
                                         i.EventName,
                                         i.StartDate,
                                         i.EndDate,
                                         i.Organizer,
                                         i.Url,
                                         participation?.Id ?? Guid.Empty);
            })
            .ToArray();
        return new ListResult(items);
    }
}
