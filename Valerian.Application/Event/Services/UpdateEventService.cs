﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using static Valerian.Application.Event.Requests.UpdateEventRequest;

namespace Valerian.Application.Event.Services;

internal sealed class UpdateEventService(IServiceProvider services) : IAction<UpdateEventRequest, UpdateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<UpdateResult> Execute(UpdateEventRequest request)
    {
        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var error = UpdateResultState.NotFound;
        var entity = await context.Events
            .SingleOrDefaultAsync(p => p.Id == request.Id);

        if (entity != null)
        {
            error = UpdateResultState.Success;

            entity.EventName = request.Item.EventName;
            entity.StartDate = request.Item.StartDate;
            entity.EndDate = request.Item.EndDate;
            entity.Organizer = request.Item.Organizer;
            entity.Url = request.Item.Url;
            entity.WeeklyId = request.Item.WeeklyId;

            var eventDate = entity.StartDate.ToDateOnly();
            var participation = await context.Participations
                .SingleOrDefaultAsync(p => p.Id == request.Id);

            participation ??= await context.Participations
                .SingleOrDefaultAsync(p => p.EventName == entity.EventName && p.EventDate == eventDate);

            if (participation != null)
            {
                participation.Id = entity.Id;
                participation.EventName = entity.EventName;
                participation.EventDate = eventDate;
                participation.Organizer = entity.Organizer;
            }
            await context.SaveChangesAsync();
        }

        return new UpdateResult(error);
    }
}
