﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Common.Extensions;
using Valerian.Data;
using Valerian.Data.Event;
using static Valerian.Application.Event.Requests.CreateEventRequest;

namespace Valerian.Application.Event.Services;

internal sealed class CreateEventService(IServiceProvider services) : IAction<CreateEventRequest, CreateResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<CreateResult> Execute(CreateEventRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.Item.EventName))
        {
            return new CreateResult(CreateResultState.EventNameRequired, Guid.Empty);
        }

        using var scope = _services.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();
        var state = CreateResultState.AlreadyEntered;
        var entity = await context.Events
            .SingleOrDefaultAsync(p => p.EventName == request.Item.EventName && p.StartDate == request.Item.StartDate);

        if (entity == null)
        {
            state = CreateResultState.Success;
            entity = _services.GetRequiredService<EventEntity>();

            entity.EventName = request.Item.EventName;
            entity.StartDate = request.Item.StartDate;
            entity.EndDate = request.Item.EndDate;
            entity.Organizer = request.Item.Organizer;
            entity.Url = request.Item.Url;
            entity.WeeklyId = request.Item.WeeklyId;

            var eventDate = entity.StartDate.ToDateOnly();
            var participation = await context.Participations
                .SingleOrDefaultAsync(p => p.EventName == entity.EventName && p.EventDate == eventDate);

            if (participation != null)
            {
                entity.Id = participation.Id;
                participation.EventName = entity.EventName;
                participation.EventDate = eventDate;
                participation.Organizer = entity.Organizer;
            }

            context.Events.Add(entity);
            await context.SaveChangesAsync();
        }

        return new CreateResult(state, entity.Id);
    }
}
