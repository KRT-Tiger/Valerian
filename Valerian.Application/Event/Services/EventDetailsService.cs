﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Valerian.Application.Event.Models;
using Valerian.Application.Event.Requests;
using Valerian.Common.Actions;
using Valerian.Data;
using static Valerian.Application.Event.Requests.EventDetailsRequest;

namespace Valerian.Application.Event.Services;

internal sealed class EventDetailsService(IServiceProvider services) : IAction<EventDetailsRequest, DetailsResult>
{
    private readonly IServiceProvider _services = services;

    public async Task<DetailsResult> Execute(EventDetailsRequest request)
    {
        using var scope = _services.CreateAsyncScope();
        var context = scope.ServiceProvider.GetRequiredService<IDataContext>();

        var state = DetailsResultState.NotFound;
        var item = EventDetailsItem.Empty;
        var entity = await context.Events
            .SingleOrDefaultAsync(p => p.Id == request.Id);
        var participation = await context.Participations
            .SingleOrDefaultAsync(i => i.Id == request.Id);

        if (entity != null)
        {
            state = DetailsResultState.Success;
            item = new EventDetailsItem(entity.EventName,
                                        entity.StartDate,
                                        entity.EndDate,
                                        entity.Organizer,
                                        entity.Url,
                                        entity.WeeklyId,
                                        participation?.Id ?? Guid.Empty);
        }

        return new DetailsResult(state, item);
    }
}
