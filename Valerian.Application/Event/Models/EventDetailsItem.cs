﻿using Valerian.Data.Event;

namespace Valerian.Application.Event.Models;

public sealed record EventDetailsItem : IEventData
{
    public static readonly EventDetailsItem Empty = new(string.Empty,
                                                        DateTime.MinValue,
                                                        DateTime.MaxValue,
                                                        string.Empty,
                                                        string.Empty,
                                                        Guid.Empty,
                                                        Guid.Empty);

    private readonly string _eventName;
    private readonly DateTime _startDate;
    private readonly DateTime _endDate;
    private readonly string _organizer;
    private readonly string _url;
    private readonly Guid _weeklyId;
    private readonly Guid _participationId;

    public EventDetailsItem(string eventName,
                            DateTime startDate,
                            DateTime endDate,
                            string organizer,
                            string url,
                            Guid weeklyId,
                            Guid participationId)
    {
        _eventName = eventName;
        _startDate = startDate;
        _endDate = endDate;
        _organizer = organizer;
        _url = url;
        _weeklyId = weeklyId;
        _participationId = participationId;
    }

    public string EventName => _eventName;
    public DateTime StartDate => _startDate;
    public DateTime EndDate => _endDate;
    public string Organizer => _organizer;
    public string Url => _url;
    public Guid WeeklyId => _weeklyId;
    public Guid ParticipationId => _participationId;
}
