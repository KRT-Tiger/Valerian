﻿namespace Valerian.Application.Event.Models;

public sealed record EventListItem
{
    private readonly Guid _id;
    private readonly string _eventName;
    private readonly DateTime _startDate;
    private readonly DateTime _endDate;
    private readonly string _organizer;
    private readonly string _url;
    private readonly Guid _participationId;

    public EventListItem(Guid id,
                         string eventName,
                         DateTime startDate,
                         DateTime endDate,
                         string organizer,
                         string url,
                         Guid participationId)
    {
        _id = id;
        _eventName = eventName;
        _startDate = startDate;
        _endDate = endDate;
        _organizer = organizer;
        _url = url;
        _participationId = participationId;
    }

    public Guid Id => _id;
    public string EventName => _eventName;
    public DateTime StartDate => _startDate;
    public DateTime EndDate => _endDate;
    public string Organizer => _organizer;
    public string Url => _url;
    public Guid ParticipationId => _participationId;
}
