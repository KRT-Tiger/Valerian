﻿using Valerian.Data.Event;

namespace Valerian.Application.Event.Models;

public sealed class EventPreviewItem(Guid id,
                                     string eventName,
                                     string organizer,
                                     DateTime startDate,
                                     DateTime endDate,
                                     string url,
                                     Guid weeklyId,
                                     Guid participationId) : IEventData
{
    public static readonly EventPreviewItem Empty = new(Guid.Empty,
                                                        string.Empty,
                                                        string.Empty,
                                                        DateTime.MinValue,
                                                        DateTime.MaxValue,
                                                        string.Empty,
                                                        Guid.Empty,
                                                        Guid.Empty);

    private readonly Guid _id = id;
    private readonly string _eventName = eventName;
    private readonly string _organizer = organizer;
    private readonly DateTime _startDate = startDate;
    private readonly DateTime _endDate = endDate;
    private readonly string _url = url;
    private readonly Guid _weeklyId = weeklyId;
    private readonly Guid _participationId = participationId;

    public Guid Id => _id;
    public string EventName => _eventName;
    public string Organizer => _organizer;
    public DateTime StartDate => _startDate;
    public DateTime EndDate => _endDate;
    public string Url => _url;
    public Guid WeeklyId => _weeklyId;
    public Guid ParticipationId => _participationId;
}
