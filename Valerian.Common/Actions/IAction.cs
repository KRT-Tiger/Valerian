﻿using Valerian.Common.Requests;

namespace Valerian.Common.Actions;

public interface IAction<TRequest> where TRequest : IRequest
{
    Task Execute(TRequest request);
}

public interface IAction<TRequest, TResponse> where TRequest : IRequest<TResponse>
{
    Task<TResponse> Execute(TRequest request);
}