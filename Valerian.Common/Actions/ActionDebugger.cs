﻿using System.Diagnostics;
using Valerian.Common.Requests;

namespace Valerian.Common.Actions;

internal sealed class ActionDebugger<TRequest>(IAction<TRequest> action) : ActionDecorator<TRequest>(action)
    where TRequest : IRequest
{
    public async override Task Execute(TRequest request)
    {
        Debug.WriteLine("Action gestartet");
        await base.Execute(request);
        Debug.WriteLine("Action beendet");
    }
}

internal sealed class ActionDebugger<TRequest, TResponse>(IAction<TRequest, TResponse> action) : ActionDecorator<TRequest, TResponse>(action)
    where TRequest : IRequest<TResponse>
{
    public async override Task<TResponse> Execute(TRequest request)
    {
        Debug.WriteLine("Action gestartet");
        var result = await base.Execute(request);
        Debug.WriteLine("Action beendet");
        return result;
    }
}

public static class ActionDebugger
{
    public static IAction<TRequest> Create<TRequest>(IAction<TRequest> action)
        where TRequest : IRequest
    {
        return new ActionDebugger<TRequest>(action);
    }

    public static IAction<TRequest, TResponse> Create<TRequest, TResponse>(IAction<TRequest, TResponse> action)
        where TRequest : IRequest<TResponse>
    {
        return new ActionDebugger<TRequest, TResponse>(action);
    }
}
