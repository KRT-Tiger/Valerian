﻿using Valerian.Common.Requests;

namespace Valerian.Common.Actions;

internal sealed class ActionProxy<TRequest>(Func<TRequest, Task> action) : IAction<TRequest>
    where TRequest : IRequest
{
    readonly Func<TRequest, Task> _action = action;

    public Task Execute(TRequest request)
    {
        return _action.Invoke(request);
    }
}

internal sealed class ActionProxy<TRequest, TResponse>(Func<TRequest, Task<TResponse>> action) : IAction<TRequest, TResponse>
    where TRequest : IRequest<TResponse>
{
    readonly Func<TRequest, Task<TResponse>> _action = action;

    public Task<TResponse> Execute(TRequest request)
    {
        return _action.Invoke(request);
    }
}

public static class ActionProxy
{
    public static IAction<TRequest> Create<TRequest>(Func<TRequest, Task> action)
        where TRequest : IRequest
    {
        return new ActionProxy<TRequest>(action);
    }

    public static IAction<TRequest, TResponse> Create<TRequest, TResponse>(Func<TRequest, Task<TResponse>> action)
        where TRequest : IRequest<TResponse>
    {
        return new ActionProxy<TRequest, TResponse>(action);
    }
}
