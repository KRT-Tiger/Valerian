﻿using Valerian.Common.Requests;

namespace Valerian.Common.Actions;

public class ActionDecorator<TRequest>(IAction<TRequest> action) : IAction<TRequest>
    where TRequest : IRequest
{
    readonly IAction<TRequest> _action = action;

    public virtual Task Execute(TRequest request)
    {
        return _action.Execute(request);
    }
}

public class ActionDecorator<TRequest, TResponse>(IAction<TRequest, TResponse> action) : IAction<TRequest, TResponse>
    where TRequest : IRequest<TResponse>
{
    readonly IAction<TRequest, TResponse> _action = action;

    public virtual Task<TResponse> Execute(TRequest request)
    {
        return _action.Execute(request);
    }
}

public static class ActionDecorator
{
    public static IAction<TRequest> Create<TRequest>(IAction<TRequest> action)
        where TRequest : IRequest
    {
        return new ActionDecorator<TRequest>(action);
    }

    public static IAction<TRequest, TResponse> Create<TRequest, TResponse>(IAction<TRequest, TResponse> action)
        where TRequest : IRequest<TResponse>
    {
        return new ActionDecorator<TRequest, TResponse>(action);
    }
}