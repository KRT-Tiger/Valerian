﻿using System.Runtime.InteropServices;

namespace Valerian.Common.Extensions;

public static class DateTimeExtensions
{
    public static DateTime BeginOfMonth(this DateTime date)
    {
        return new DateTime(date.Year, date.Month, 1);
    }

    public static DateTime EndOfMonth(this DateTime date)
    {
        return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
    }

    public static string ToDateString(this DateTime date, DateStringFormat format)
    {
        switch(format)
        {
            case DateStringFormat.Date:
                return date.ToString("dd.MM.yy");
            case DateStringFormat.Time:
                return date.ToString("HH:mm");
            case DateStringFormat.DateTime:
                return date.ToString("dd.MM.yy HH:mm");
        }
        throw new NotImplementedException(format.ToString());
    }
}

public enum DateStringFormat
{
    Date,
    Time,
    DateTime
}
