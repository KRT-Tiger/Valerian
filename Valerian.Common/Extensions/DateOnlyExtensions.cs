﻿namespace Valerian.Common.Extensions;

public static class DateOnlyExtensions
{
    public static string ToDateString(this DateOnly date)
    {
        return date.ToString("dd.MM.yyyy");
    }

    public static DateOnly ToDateOnly(this DateTime date)
    {
        return DateOnly.FromDateTime(date);
    }
}
