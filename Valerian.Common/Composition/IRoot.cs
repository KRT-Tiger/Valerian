﻿using Microsoft.Extensions.DependencyInjection;

namespace Valerian.Common.Composition;

public interface IRoot
{
    void Compose(IServiceCollection services);
}
