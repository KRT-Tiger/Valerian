﻿using Microsoft.Extensions.DependencyInjection;

namespace Valerian.Common.Composition;

public static class RootExtensions
{
    public static IRoot Combine(this IRoot self, IRoot root, params IRoot[] other)
    {
        var roots = new List<IRoot>
        {
            self,
            root
        };
        roots.AddRange(other);
        return new CombinedRoot([.. roots]);
    }

    private sealed class CombinedRoot(IRoot[] roots) : IRoot
    {
        private readonly IRoot[] _roots = roots;

        public void Compose(IServiceCollection services)
        {
            foreach (var root in _roots)
            {
                root.Compose(services);
            }
        }
    }
}
