﻿using System.Security.Principal;

namespace Valerian.Common.Requests;

public record Request : IRequest
{
    readonly IIdentity _user;

    public Request(IIdentity user)
    {
        _user = user;
    }

    public IIdentity User => _user;
}
