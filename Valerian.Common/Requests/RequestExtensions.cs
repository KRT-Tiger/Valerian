﻿using Valerian.Common.Actions;

namespace Valerian.Common.Requests;

public static class RequestExtensions
{
    public async static Task With<TRequest>(this TRequest request, IAction<TRequest> action)
        where TRequest : IRequest
    {
        await action.Execute(request);
    }

    public static Task<TResponse> With<TRequest, TResponse>(this TRequest request, IAction<TRequest, TResponse> action)
        where TRequest : IRequest<TResponse>
    {
        return action.Execute(request);
    }

    public async static Task With<TRequest>(this Task<TRequest> request, IAction<TRequest> action)
        where TRequest : IRequest
    {
        await action.Execute(await request);
    }

    public async static Task<TResponse> With<TRequest, TResponse>(this Task<TRequest> request, IAction<TRequest, TResponse> action)
        where TRequest : IRequest<TResponse>
    {
        var result = await action.Execute(await request);
        return result;
    }
}
