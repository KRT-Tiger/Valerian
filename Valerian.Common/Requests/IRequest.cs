﻿using System.Security.Principal;

namespace Valerian.Common.Requests;

public interface IRequest
{
    IIdentity User { get; }
}

public interface IRequest<TResult> : IRequest
{
}
